/*
 * Copyright 2020 Inivata Inc.
 * Copyright 2020 Inivata Ltd.
 *
 * Example usage: ./read-qc -B testdata/test.bam -b testdata/test.bed 
 *   -F testdata/test.fa
 */

#include <getopt.h>
#include <stdio.h>
#include "src/primer_qc.h"


void printUsage() {
  fprintf(stderr, "\n[panel-qc] Usage:\n");
  fprintf(stderr, "\t-m\tminimum mapping quality (default 30)\n");
  fprintf(stderr, "\t-q\tminimum base quality (default 30)\n");
  fprintf(stderr, "\t-f\tnumber of flanking bases (default 50)\n");
  fprintf(stderr, "\t-B\tBAM file\n");
  fprintf(stderr, "\t-b\tBED file\n");
  fprintf(stderr, "\t-v\tverbose (0 default, 1 print out queries with where neither read 1 nor 2 has a known mapping)\n");
  fprintf(stderr, "\t-h\tHelp\n");
  exit(1);
}


int main(int argc, char **argv) {
  if (argc == 1) {
    printUsage();
  }
  int min_map_qual = 30;
  int min_base_qual = 30;
  int flank = 50;
  const char* bam = NULL;
  const char* bed = NULL;
  int verbose = 0;
  int opt;
  while ((opt = getopt(argc, argv, "m:q:f:B:b:v:h")) >= 0) {
    switch (opt) {
      case 'm':
        min_map_qual = std::stoi(optarg);
        break;
      case 'q':
        min_base_qual = std::stoi(optarg);
        break;
      case 'f':
        flank = std::stoi(optarg);
        break;
      case 'B':
        bam = optarg;
        break;
      case 'b':
        bed = optarg;
        break;
      case 'v':
        verbose = std::stoi(optarg);
        break;
      case 'h':
        printUsage();
        break;
      default:
        printUsage();
        break;
    }
  }
  if ((bam == NULL) || (bed == NULL) ) {
    printUsage();
  }
  PrimerQC primer_qc;
  primer_qc.initialise(bam, bed, min_map_qual, min_base_qual, flank, verbose);
  primer_qc.buildTable();
  primer_qc.write();
  return 0;
}
