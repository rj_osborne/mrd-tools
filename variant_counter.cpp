/*
 * Copyright 2020 Inivata Inc.
 * Copyright 2020 Inivata Ltd.
 *
 */

#include <getopt.h>
#include <stdio.h>
#include "src/bam_util.h"
#include "src/bed_util.h"
#include "src/fasta_reader.h"
#include "src/entry.h"


void printUsage() {
  fprintf(stderr, "\n[variant-counter] Usage:\n");
  fprintf(stderr, "\t-f\tnumber of flanking bases (default 50)\n");
  fprintf(stderr, "\t-q\tminimum mapping quality (default 30)\n");
  fprintf(stderr, "\t-B\tBAM file\n");
  fprintf(stderr, "\t-b\tBED file\n");
  fprintf(stderr, "\t-F\tFASTA file\n");
  fprintf(stderr, "\t-h\tHelp\n");
  exit(1);
}


void variantCounter(const char* bam, const char* bed, const char* fasta, 
  int flank, int min_map_qual) {
  // initialise bam
  BamUtil bam_util;
  bam_util.open(bam);
  bam_util.loadHeader();
  bam_util.loadIndex();
  // initialise fasta reader
  FastaReader fasta_reader;
  fasta_reader.open(fasta);
  // initialise bed_util
  BedUtil bed_util;
  bed_util.open(bed);
  bed_util.read();
  bed_util.extendFlanks(bam_util, flank);
  bed_util.sequencesFromFasta(fasta_reader);
  std::cout << bam_util.writeVariantHeader() << std::endl;
  // iterate through bed entries
  for (int i = 0; i < bed_util.entries.size(); i++) {
    entry_t entry = bed_util.entries[i];
    bam_util.initialisePileup(&entry);
    int result;
    do {
      result = bam_util.nextColumn();
      bam_util.pileupEntry(&entry, min_map_qual);
    } while (result > -1);
  }
}


int main(int argc, char **argv) {
  if (argc == 1) {
    printUsage();
  }
  int flank = 50;
  int min_map_qual = 30;
  const char* bam = NULL;
  const char* bed = NULL;
  const char* fasta = NULL;
  int opt;
  while ((opt = getopt(argc, argv, "f:q:B:b:F:h")) >= 0) {
    switch (opt) {
      case 'f':
        try {
          flank = std::stoi(optarg);
        }
        catch (const std::invalid_argument& ia) {
          fprintf(stderr, "\nWarning: -f specifies the number of flanking"
            " bases and not the FASTA file name\n");
          printUsage();
        }
        break;
      case 'q':
        min_map_qual = std::stoi(optarg);
        break;
      case 'B':
        bam = optarg;
        break;
      case 'b':
        bed = optarg;
        break;
      case 'F':
        fasta = optarg;
        break;
      case 'h':
        printUsage();
        break;
      default:
        printUsage();
        break;
    }
  }
  if ((bam == NULL) || (bed == NULL) || (fasta == NULL)) {
    printUsage();
  }
  variantCounter(bam, bed, fasta, flank, min_map_qual);
  return 0;
}
