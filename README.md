# Tools for analysis of MRD data

#### Installation
Clone directory from bitbucket
cd mrd-tools
make

#### Panel QC
##### Run
./panel_qc 
[panel-qc] Usage:
&nbsp;	&nbsp; -m	minimum mapping quality (default 30)
&nbsp;	&nbsp; -q	minimum base quality (default 30)
&nbsp;	&nbsp; -f	number of flanking bases (default 50)
&nbsp;	&nbsp; -B	BAM file
&nbsp;	&nbsp; -b	BED file
&nbsp;	&nbsp; -h	Help

##### Input
BAM file is generated using bwa mem on unprocessed fastq files. Do not sort or index the resulting BAM file - the panel_qc script requires that reads are in read name (and not coordinate) order.
BED file must have 6 columns: reference name, beginning coordinate, end coordinate, primer/amplicon name, forward primer sequence, reverse primer sequence.
The script assesses the number of reads with mapping quality ≥ to a minimum  mapping quality (set by -m), and the number of bases ≥ to a minimum base quality (set by -q). 
If required, flanking bases can be added to the expected amplicon coordinates.

##### Output
A csv file with the following headings is written to stdout:
* forward : forward primer match
* reverse : reverse primer match
* is_usable : True if reads are paired but not supplementary, not unmapped, not secondary, and not QC fails. 
* overlap_type : 
  * 0 : either read 1 or read 2 are not mapped
  * 1 : either read 1 or read 2 do not match a known primer
  * 2 : reads 1 and 2 both match known primers but primer1 and primer2 do not match
  * 3 : primer 1 and primer 2 match, either one or other read does not overlap the expected target
  * 4 : primer 1 and primer 2 match, both reads overlap the expected target
* mapping_quality >= threshold: number of reads with mapping quality ≥ threshold
* mapping_quality < threshold: number of reads with mapping quality < threshold
* base_quality >= threshold: number of bases with base quality ≥ threshold
* base_quality < threshold: number of bases with base quality < threshold


#### Variant counting
##### Run
./variant_counter 
[variant-counter] Usage:
&nbsp;	&nbsp; -f	number of flanking bases (default 50)
&nbsp;	&nbsp; -q	minimum mapping quality (default 30)
&nbsp;	&nbsp; -B	BAM file
&nbsp;	&nbsp; -b	BED file
&nbsp;	&nbsp; -F	FASTA file
&nbsp;	&nbsp; -h	Help

##### Input
BAM file is generated using bwa mem on unprocessed fastq files. Coordinate sort and index the resulting BAM file.
BED file must have 4 columns: reference name, beginning coordinate, end coordinate, primer/amplicon name.
FASTA file is the reference genome used for alignment.
If minimum mapping quality is set at a value > 0 then only reads with mapping quality ≥ threshold are included for analysis.
If required, flanking bases can be added to the expected amplicon coordinates.

##### Output
A csv file with the following headings is written to stdout:
* amplicon name
* reference name
* coordinate
* reference base
* base call from forward reads
* base call from reverse reads
* counts of forward bases with quality values from 0 (deletions) to 41: [fq0, fq1 ... fq41]
* counts of reverse bases with quality values from 0 (deletions) to 41: [rq0, rq1 ... rq41]

Note that bases not reads are counted. For example, if a read has a 2 bp insertion or deletion then all 2 bases will be counted.