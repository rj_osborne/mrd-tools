/*
 * Copyright 2020 Inivata Inc.
 * Copyright 2020 Inivata Ltd.
 */


#include <assert.h>
#include "bed_util.h"


// nextEntry,.coordinate


void testBedUtil() {

  int flank = 150;

  // initialise bam
  BamUtil bam_util;
  bam_util.open("testdata/test.bam");
  bam_util.loadHeader();

  // initialise fasta reader
  FastaReader fasta_reader;
  fasta_reader.open("testdata/test.fa");

  // initialise bed util
  BedUtil bed_util;
  bed_util.open("testdata/test.bed");
  bed_util.read();
  assert (bed_util.entries.size() == 3);

  // check expected results
  assert(bed_util.header.compare("#header") == 0);
  assert(bed_util.entries[0].rname.compare("chr1") == 0);
  assert(bed_util.entries[1].rname.compare("chr2") == 0);
  assert(bed_util.entries[2].rname.compare("chrX") == 0);
  assert(bed_util.entries[0].beg == 0);
  assert(bed_util.entries[1].beg == 30);
  assert(bed_util.entries[2].beg == 100);  
  assert(bed_util.entries[0].end == 60);
  assert(bed_util.entries[1].end == 80);
  assert(bed_util.entries[2].end == 150);  
  assert(bed_util.entries[0].id.compare("interval1") == 0);
  assert(bed_util.entries[1].id.compare("interval2") == 0);
  assert(bed_util.entries[2].id.compare("interval3") == 0);

  bed_util.index();
  assert (bed_util.idx.size() == 3);
  assert (bed_util.idx["interval1"].beg == 0);
  assert (bed_util.idx["interval2"].beg == 30);
  assert (bed_util.idx["interval3"].beg == 100);

  // add beg_incl_flank and end_incl_flank coordinates
  bed_util.extendFlanks(bam_util, flank);

  // reference sequence = 69 bp
  assert (bed_util.entries[0].beg == 0);
  assert (bed_util.entries[0].end == 60);
  assert (bed_util.entries[0].beg_incl_flank == 1);
  assert (bed_util.entries[0].end_incl_flank == 69);


  // reference sequence =  280 bp
  assert (bed_util.entries[1].beg == 30);
  assert (bed_util.entries[1].end == 80);
  assert (bed_util.entries[1].beg_incl_flank == 1);
  assert (bed_util.entries[1].end_incl_flank == 230);

  // reference sequence =  280 bp
  assert (bed_util.entries[2].beg == 100);
  assert (bed_util.entries[2].end == 150);
  assert (bed_util.entries[2].beg_incl_flank == 1);
  assert (bed_util.entries[2].end_incl_flank == 280);

  // add sequences including flanks
  bed_util.sequencesFromFasta(fasta_reader);

  assert (bed_util.entries[0].coord_incl_flank.compare("chr1:1-69") == 0);
  assert (bed_util.entries[1].coord_incl_flank.compare("chr2:1-230") == 0);
  assert (bed_util.entries[2].coord_incl_flank.compare("chrX:1-280") == 0);

  char sequence1[] = "AAAAACACAGAGACACAGAGACACAGAAGAGGCAAACAGATATAGGTAGAGAGAC"
    "ACACAGTTTGAGAG";
  assert (strcmp(sequence1, bed_util.entries[0].sequence_incl_flank) == 0);

  char sequence2[] = "GTTATGACTTTTGTACAGACCTTTACACCTAGGTATTCTGAAGTTGGTGGTCTGTG"
    "AGTGGCATCGAGTGATGACTGACACACTCTGACCTGGGGTAGAACAACACGTCTCCCTGCAGTTTGCTGAATT"
    "TTAGGGCGTCACCACCTGTTTTCAAGAGTGTGTTTTTCTATCCTCCCGAGTTTTGCCCACCTAAGCAATGGTT"
    "TTGCTGTAATAAAGAATTACACTATTTA";
  assert (strcmp(sequence2, bed_util.entries[1].sequence_incl_flank) == 0);

  char sequence3[] = "CCCTAACCCTAACCCTAACCCTAACCCTCTGAAAGTGGACCTATCAGCAGGATGTG"
    "GGTGGGAGCAGATTAGAGAATAAAAGCAGACTGCCTGAGCCAGCAGTGGCAACCCAATGGGGTCCCTTTCCAT"
    "ACTGTGGAAGCTTCGTTCTTTCACTCTTTGCAATAAATCTTGCTATTGCTCACTCTTTGGGTCCACACTGCCT"
    "TTATGAGCTGTGACACTCACCGCAAAGGTCTGCAGCTTCACTCCTGAGCCAGTGAGACCACAACCCCACCAGA"
    "AAGAA";
  assert (strcmp(sequence3, bed_util.entries[2].sequence_incl_flank) == 0);

  bed_util.close();

}


int main(int argc, char **argv) {
  testBedUtil();
  std::cout << "[bed_util test] passed" << std::endl;
}
