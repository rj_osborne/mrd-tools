/*
 * Copyright 2020 Inivata Inc.
 * Copyright 2020 Inivata Ltd.
 */

#include <assert.h>
#include <iostream>
#include "primer_qc.h"


/*
 *  Test overlapType
 */
void test1() {
  PrimerQC primer_qc;
  const char* bam;
  bam = "testdata/test.unsorted.bam";
  primer_qc.bam_util.open(bam);
  primer_qc.bam_util.loadHeader();
  primer_qc.bam_util.initialiseAlignment();

  read_t read1, read2;
  std::string primer1, primer2;
  entry_t entry;

  read1.rname = "unknown";
  read2.rname = "unknown";
  assert (primer_qc.overlapType(read1, primer1, read2, primer2, entry) == 0);
  read1.rname = "interval1";
  assert (primer_qc.overlapType(read1, primer1, read2, primer2, entry) == 0);
  read2.rname = "interval1";
  assert (primer_qc.overlapType(read1, primer1, read2, primer2, entry) != 0);
  
  primer1 = "unknown";
  primer2 = "unknown";
  assert (primer_qc.overlapType(read1, primer1, read2, primer2, entry) == 1);
  primer1 = "interval1_F";
  assert (primer_qc.overlapType(read1, primer1, read2, primer2, entry) == 1);
  primer2 = "interval1_R";
  assert (primer_qc.overlapType(read1, primer1, read2, primer2, entry) != 1);

  assert (primer_qc.overlapType(read1, primer1, read2, primer2, entry) != 2);
  primer2 = "interval2_F";
  assert (primer_qc.overlapType(read1, primer1, read2, primer2, entry) == 2);
  primer2 = "interval1_R";

  entry.beg_incl_flank = 10;
  entry.end_incl_flank = 100;

  read1.beg = 10;
  read1.end = 12;
  read2.beg = 98;
  read2.end = 100;
  assert (primer_qc.overlapType(read1, primer1, read2, primer2, entry) == 4);

  read1.beg = 9;
  read1.end = 11;
  read2.beg = 99;
  read2.end = 101;
  assert (primer_qc.overlapType(read1, primer1, read2, primer2, entry) == 4);

  read1.beg = 1;
  read1.end = 10;
  read2.beg = 100;
  read2.end = 110;
  assert (primer_qc.overlapType(read1, primer1, read2, primer2, entry) == 4);

  read1.beg = 1;
  read1.end = 9;
  read2.beg = 101;
  read2.end = 110;
  assert (primer_qc.overlapType(read1, primer1, read2, primer2, entry) == 3);
}


/*
 *  Test header
 */
void test2() {
  PrimerQC primer_qc;
  primer_qc.mmq = 10;
  primer_qc.mbq = 11;
  std::string header = "forward,reverse,is_usable,overlap_type,"
    "mapping_quality>=10,mapping_quality<10,base_quality>=11,base_quality<11";
  assert (primer_qc.header().compare(header) == 0);
}


/*
 *  Test initialise, buildTrie
 */
void test3() {
  PrimerQC primer_qc;
  const char* bam;
  const char* bed;
  bam = "testdata/test.unsorted.bam";
  bed = "testdata/test.bed";
  int min_map_qual, min_base_qual, flank;
  int verbose = 0;

  primer_qc.initialise(bam, bed, min_map_qual, min_base_qual, flank, verbose);
  primer_qc.buildTrie();

  assert (primer_qc.trie.search("ACAGAAGAGG").compare("interval1_F") == 0);
  assert (primer_qc.trie.search("ACAGAAGAGGAAGAGAGAGAC").compare(
    "interval1_F") == 0);
  assert (primer_qc.trie.search("GGGGGGGGG").compare("unknown") == 0);
  assert (primer_qc.trie.search("TATATCTGTT").compare("interval1_R") == 0);
  assert (primer_qc.trie.search("TATATCTGTTAACACAAGA").compare(
    "interval1_R") == 0);
  assert (primer_qc.trie.search("CCCCCCCCC").compare("unknown") == 0);
}


/*
 *  Test initialise, buildTables, getData
 */
void test4() {
  PrimerQC primer_qc;
  const char* bam;
  const char* bed;
  bam = "testdata/test.unsorted.bam";
  bed = "testdata/test.bed";
  int min_map_qual = 30;
  int min_base_qual = 30;
  int flank = 0;
  int verbose = 0;
  primer_qc.initialise(bam, bed, min_map_qual, min_base_qual, flank, verbose);
  primer_qc.buildTable();

  std::vector<std::string> results = primer_qc.getData();

  assert (results.size() == 3);
  assert (results[0].compare("interval1_F,interval1_F,0,4,0,8,88,88") == 0);
  assert (results[1].compare("interval1_F,interval1_R,1,4,2,6,93,93") == 0);
  assert (results[2].compare("interval1_R,interval1_F,1,4,2,6,85,85") == 0);
}


int main(int argc, char **argv) {
  test1();
  test2();
  test3();
  test4();
  std::cout << "[primer_qc test] passed" << std::endl;
}
