/*
 * Copyright 2020 Inivata Inc.
 * Copyright 2020 Inivata Ltd.
 */

#include "fasta_reader.h"


/**
 * Initialise fasta file
 * @param fasta : fasta filename
 */
void FastaReader::open(const char* fasta) {
  this->fai = fai_load(fasta);
}


/**
* Get the sequence for a genomic interval
* @param coordinate : string in rname:beg-end format
* @return string : upper case reference sequence
*/
char* FastaReader::sequence(std::string coord) {
  int length;
  char* sequence = fai_fetch(this->fai, coord.c_str(), &length);
  if (sequence == NULL) {
    std::stringstream er;
    er << "Error: failed to fetch sequence from ";
    er << coord;
    er << std::endl;
    throw std::runtime_error(er.str());
  }
  for (int i = 0; i < strlen(sequence); i++) {
    sequence[i] = toupper(sequence[i]);
  }
  return sequence;
}


int FastaReader::referenceLength(std::string rname) {
  return faidx_seq_len(this->fai, rname.c_str());
}


void FastaReader::close() {
  fai_destroy(this->fai);
}
