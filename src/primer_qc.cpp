/*
 * Copyright 2020 Inivata Inc.
 * Copyright 2020 Inivata Ltd.
 */

#include "primer_qc.h"


/*
 * Initialise primer quality control
 * @param bam : bam filename
 * @param bed : bed filename
 * @param min_map_qual : minimum mapping quality
 * @param min_base_qual : minimum base quality
 * @param flank : number bases flanking the interval
 */
void PrimerQC::initialise(const char* bam, const char* bed, int min_map_qual,
    int min_base_qual, int flank, int verbose) {
  this->mbq = min_base_qual;
  this->mmq = min_map_qual;
  this->verbose = verbose;
  // bam
  this->bam_util.open(bam);
  this->bam_util.loadHeader();
  this->bam_util.initialiseAlignment();
  // bed
  this->bed_util.open(bed);
  this->bed_util.read();
  this->bed_util.extendFlanks(this->bam_util, flank);
  this->bed_util.index();
  // trie
  PrimerQC::buildTrie();
  // output file for unkonwn primers
  if (this->verbose == 1) {
    this->outfile.open("unknown.qnames.txt");
  }
}


/*
 * Build forward and reverse primer tries
 */
void PrimerQC::buildTrie() {
  std::map<std::string, std::string> primers;
  for (int i = 0; i < this->bed_util.entries.size(); i++) {
    entry_t entry = this->bed_util.entries[i];
    std::stringstream fwd, rev;
    fwd << entry.id << "_F";
    rev << entry.id << "_R";
    primers[fwd.str()] = entry.fwd;
    primers[rev.str()] = entry.rev;    
  }
  this->trie.build(primers);
}


/*
 * Read pair is usable
 * @param read1 : read 1 structure
 * @param read2 : read 2 structure
 * @returns boolean : true if read pair is usable
 */
int PrimerQC::pairIsUsable(read_t read1, read_t read2) {
  if (((read1.strand + read2.strand) == 1) &&
      (read1.is_usable == true) &&
      (read2.is_usable == true)) {
    return 1;
  } 
  return 0;
}


/*
 * Run primer quality control
 */
void PrimerQC::buildTable() {
  int ret;
  while ((ret = this->bam_util.nextRepresentativeRead()) >= 0) {
    // first read in pair
    read_t read1 = this->bam_util.readTable(this->mbq, this->mmq);
    std::string primer1 = PrimerQC::search();
    // second read in pair
    this->bam_util.nextRepresentativeRead();
    read_t read2 = this->bam_util.readTable(this->mbq, this->mmq);
    std::string primer2 = PrimerQC::search();
    // check reads are in sync
    assert (read1.read_no == 1);
    assert (read2.read_no == 2);
    assert (read1.qname.compare(read2.qname) == 0);
    // temporary qc step for Gio
    if ((this->verbose == 1) &&
        (primer1.compare("unknown") == 0) && 
        (primer2.compare("unknown") == 0)) {
      this->outfile << read1.qname << std::endl;
    }
    // build key
    entry_t entry = this->bed_util.idx[primer1.substr(0, primer1.size()-2)];
    std::stringstream ss; 
    ss << primer1 << "," << primer2 << ","
       << PrimerQC::pairIsUsable(read1, read2) << ","
       << PrimerQC::overlapType(read1, primer1, read2, primer2, entry);
    std::string key = ss.str();
    // count mapping qualities
    this->counts[key].mqs[read1.mq]++;
    this->counts[key].mqs[read2.mq]++;
    // count base qualities
    for (auto it = read1.bq.begin(); it != read1.bq.end(); it++) {
      this->counts[key].bqs[it->first] += it->second;
    }
    for (auto it = read2.bq.begin(); it != read2.bq.end(); it++) {
      this->counts[key].bqs[it->first] += it->second;
    }  
  }
  // close this->outfile
  this->outfile.close();
}


/*
 * Return primer name if prefix has a match in the trie
 */
std::string PrimerQC::search() {
  std::string prefix;
  // read is mapped to reverse strand
  if (this->bam_util.readStrand() == 1) {
    prefix = bam_util.revPrefix(this->trie.depth);
  }
  // read is mapped to forward strand or no strand is defined
  else {
    prefix = bam_util.fwdPrefix(this->trie.depth);
  }
  return this->trie.search(prefix);
}


/*
 * Overlap between expected target coordinates and read coordinates
 * @param read1 : read 1 structure
 * @param primer1 : matching primer 1 name
 * @param read2 : read 2 structure
 * @param primer2 : matching primer 2 name
 * @returns overlap type
 *   0 : either read 1 or read 2 are not mapped
 *   1 : either read 1 or read 2 do not match a known primer
 *   2 : reads 1 and 2 both match known primers but primer1 and primer2 do 
 *       not match
 *   3 : primer 1 and primer 2 match, either one or other read does not overlap
 *       the expected target
 *   4 : primer 1 and primer 2 match, both reads overlap the expected target
 */
int PrimerQC::overlapType(read_t read1, std::string primer1,
    read_t read2, std::string primer2, entry_t entry) {
  // either read1 or read2 has no mapping
  if ((read1.rname.compare("unknown") == 0) ||
      (read2.rname.compare("unknown") == 0)) {
    return 0;
  }
  // read1 or read2 do not match a known primer
  if ((primer1.compare("unknown") == 0) || (primer2.compare("unknown") == 0)) {
    return 1;
  }
  // primer1 and primer2 do not match each other
  primer1 = primer1.substr(0, primer1.size()-2);
  primer2 = primer2.substr(0, primer2.size()-2);
  if (primer1.compare(primer2) != 0) {
    return 2;
  }
  // compare overlap : https://stackoverflow.com/questions/3269434
  if ((entry.beg_incl_flank <= read1.end) &&
      (read1.beg <= entry.end_incl_flank) &&
      (entry.beg_incl_flank <= read2.end) &&
      (read2.beg <= entry.end_incl_flank)) {
    return 4;
  }
  else {
    return 3;
  }
}


/*
 * Return header string
 */
std::string PrimerQC::header() {
  std::stringstream ss;
  ss << "forward,reverse,is_usable,overlap_type,mapping_quality>=" << this->mmq
     << ",mapping_quality<" << this->mmq << ",base_quality>=" << this->mbq 
     << ",base_quality<" << this->mbq;
  return ss.str();
}


/*
 * Data in csv format
 *   compound key
 *   number of reads with mapping quality >= threshold
 *   number of reads with mapping quality < threshold
 *   number of bases with base quality >= threshold
 *   number of bases with base quality < threshold
 */
std::vector<std::string> PrimerQC::getData() {
  std::vector<std::string> results;
  for (auto it = this->counts.begin(); it != this->counts.end(); it++) {
    std::stringstream ss;
    ss << it->first << "," << it->second.mqs[1] << "," << it->second.mqs[0]
       << "," << it->second.bqs[1] << "," << it->second.bqs[0];
    results.push_back(ss.str());
  }
  return results;
}


/*
 * Write out data in csv format
 */
void PrimerQC::write() {
  std::cout << PrimerQC::header() << std::endl;
  std::vector<std::string> data = PrimerQC::getData();
  for (int i = 0; i < data.size(); i++) {
    std::cout << data[i] << std::endl;
  }
}
