/*
 * Copyright 2020 Inivata Inc.
 * Copyright 2020 Inivata Ltd.
 */

#ifndef BAM_UTIL_H
#define BAM_UTIL_H

#include <limits.h>
#include <iostream>
#include <map>
#include <set>
#include <stdexcept>
#include <string>
#include <sstream>
#include <vector>
#include <cassert>
#include "../third_party/htslib-1.3.1/htslib/sam.h"
#include "entry.h"


typedef struct {
  std::string bases;
  std::map<int, int> quals;
} base_t;


typedef struct {
  std::string qname;
  int read_no;
  int mq;
  std::map<int, int> bq;
  int strand;
  bool is_usable;
  std::string rname;
  int beg;
  int end;
} read_t;


// { qname : {orientation : base_t} }
typedef std::map<std::string, std::map<int, base_t>> read_index;


// { base1,base2 : { orientation : { mapq : count } } }
typedef std::map<std::string, std::map<int, std::map<int, int>>> variant_index;


class BamUtil {

  public:

    const char* bam;

    htsFile *in;

    bam_hdr_t *head;

    hts_idx_t *idx;

    bam1_t *b; 

    hts_itr_t *iter;

    bam_plp_t buf;

    void open(const char* bam);

    void close();

    void loadHeader();

    void closeHeader();

    void loadIndex();

    void closeIndex();

    void initialiseTargetIterator(std::string coord);

    void destroyTargetIterator();

    void initialiseAlignment();

    void destroyAlignment();

    int nextTargetRead();

    int nextRead();

    bool readIsSupplementary();

    int nextRepresentativeRead();

    bool readIsUsable();

    int templateIdentifier(std::string rname);

    std::string referenceName();

    std::string queryName();

    int leftCoordinate();

    int rightCoordinate();

    bool readIsForward();

    bool readIsReverse();

    int readStrand();

    int readNumber();

    int mapQuality();

    std::string fwdPrefix(int prefix_len);

    std::string revPrefix(int prefix_len);

    int referenceLength(std::string rname);

    void initialisePileup(entry_t *entry);

    int nextColumn();

    std::string bases(const bam_pileup1_t *p, int pos, entry_t *entry);

    std::map<int, int> quals(const bam_pileup1_t *p);

    void pileupEntry(entry_t *entry, int min_map_qual);

    read_index indexReads(const bam_pileup1_t *pl, int n_plp, int pos,
      entry_t *entry, int min_map_qual);

    variant_index indexVariants(read_index r_idx);

    void writeVariants(entry_t *entry, int pos, variant_index v_idx);

    std::string writeVariantHeader();

    read_t readTable(int min_base_qual, int min_map_qual);

    std::map<int,int> baseQualities(int min_base_qual);

};

#endif
