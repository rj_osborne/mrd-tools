/*
 * Copyright 2020 Inivata Inc.
 * Copyright 2020 Inivata Ltd.
 */

#ifndef PRIMER_QC_H_
#define PRIMER_QC_H_

#include <map>
#include <string>
#include <vector>
#include <iostream>
#include <fstream> 

#include "bam_util.h"
#include "bed_util.h"
#include "primer_trie.h"


typedef struct {
  std::map<int,int> mqs;
  std::map<int, int> bqs;
} count_t;


class PrimerQC {

  public:

    int mbq;

    int mmq;

    int verbose;

    std::ofstream outfile;

    BamUtil bam_util;

    BedUtil bed_util;

    PrimerTrie trie;

    std::map<std::string, std::vector<entry_t>> index;

    std::map<std::string, count_t> counts;

    void initialise(const char* bam, const char* bed, int min_map_qual,
      int min_base_qual, int flank, int verbose);

    void buildTrie();

    int pairIsUsable(read_t read1, read_t read2);

    void buildTable();

    std::string search();

    int overlapType(read_t read1, std::string primer1, read_t read2,
      std::string primer2, entry_t entry);

    std::string compoundKey(read_t rt1, read_t rt2);

    std::string header();

    std::vector<std::string> getData();

    void write();

};

#endif
