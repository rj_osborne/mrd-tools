/*
 * Copyright 2020 Inivata Inc.
 * Copyright 2020 Inivata Ltd.
 *
 */
 
#include "primer_trie.h"


/**
 * Test edits1
 */
void PrimerTrieTest1() {

  PrimerTrie primer_trie;
  std::set<std::string> observed = primer_trie.edits1("ACGT");

  std::set<std::string> expected = { 
    // substitutions
    "ACGT", "AAGT", "ACAT", "ACGA",
    "CCGT", "ACGT", "ACCT", "ACGC",
    "GCGT", "AGGT", "ACGT", "ACGG",
    "TCGT", "ATGT", "ACTT", "ACGT",
    // deletions
    "CGT", "AGT", "ACT", "ACG",
    // insertions
    "AACGT", "AACGT", "ACAGT", "ACGAT", "ACGTA",
    "CACGT", "ACCGT", "ACCGT", "ACGCT", "ACGTC",
    "GACGT", "AGCGT", "ACGGT", "ACGGT", "ACGTG",
    "TACGT", "ATCGT", "ACTGT", "ACGTT", "ACGTT" 
    };

  assert(observed == expected);
  
}


/**
 * Test edits2
 */
void PrimerTrieTest2() {

  PrimerTrie primer_trie;
  std::set<std::string> observed = primer_trie.edits2("A");

  std::set<std::string> expected = {

    // edits1: mononucleotides
    "", "A", "C", "G", "T",
    
    // edits1: dinucleotides
    "AA", "AC", "AG", "AT", "CA", "GA", "TA",

    // edits2: mononucleotides (same as for edits1)
    
    // edits2: dinucleotides (same as for edits1 but also)
    "CC", "CG", "CT",
    "GC", "GG", "GT",
    "TC", "TG", "TT",

    // edits2: trinucleotides
    // base = AA
    "AAA", "CAA", "GAA", "TAA",
    "AAA", "ACA", "AGA", "ATA",
    "AAT", "AAC", "AAG", "AAT",
    // base = AC
    "AAC", "CAC", "GAC", "TAC",
    "AAC", "ACC", "AGC", "ATC",
    "ACA", "ACC", "ACG", "ACT",
    // base = AG
    "AAG", "CAG", "GAG", "TAG",
    "AAG", "ACG", "AGG", "ATG",
    "AGA", "AGC", "AGG", "AGT",  
    // base = AT
    "AAT", "CAT", "GAT", "TAT",
    "AAT", "ACT", "AGT", "ATT",
    "ATA", "ATC", "ATG", "ATT",  
    // base = CA
    "ACA", "CCA", "GCA", "TCA",
    "CAA", "CCA", "CGA", "CTA",
    "CAA", "CAC", "CAG", "CAT",  
    // base = GA
    "AGA", "CGA", "GGA", "TGA",
    "GAA", "GCA", "GGA", "GTA",
    "GAA", "GAC", "GAG", "GAT",  
    // base = TA
    "ATA", "CTA", "GTA", "TTA",
    "TAA", "TCA", "TGA", "TTA",
    "TAA", "TAC", "TAG", "TAT",    
  };

  assert (observed == expected);
  
}


/**
 * Tests initialise, insert, depth and search
 */
void PrimerTrieTest3() {

  PrimerTrie primer_trie;
  primer_trie.depth = 0;
  primer_trie.root = new Trie();
  primer_trie.insert("primer1", "ACGT");
  primer_trie.insert("primer2", "ACGT");
  
  assert (primer_trie.depth == 4);

  primer_trie.insert("primer2", "ACGTA");
  assert (primer_trie.depth == 5);  

  // no primer begins with "C"
  assert (primer_trie.search("C").compare("unknown") == 0);

  // ACGT matches both primer 1 and primer 2
  assert (primer_trie.search("ACGT").compare("unknown") == 0); 

  // ACGTA matches primer 2 only
  assert (primer_trie.search("ACGTA").compare("primer2") == 0);

  // primer1 & primer 2 match ACGT, primer2 matches ACGTA
  // therefore primer 2 has the longest match and is the matching primer
  assert (primer_trie.search("ACGTAA").compare("primer2") == 0);

}  


/**
 * Tests initialise, depth, search and build
 */
void PrimerTrieTest4() {
  PrimerTrie primer_trie;

  std::map<std::string, std::string> primers;
  primers["primer1"] = "ACGT";
  primers["primer2"] = "ACGTA";
  
  // trie contains all primers within an edit distance of 2, therefore the
  // maximum depth is 7 (ACGTA + 2 insertions)
  primer_trie.build(primers);
  assert (primer_trie.depth == 7);
  
  // ACGTA matches primer1 (+ 1 insert) and primer 2 (0 edits), therefore no 
  // one primer can be identified
  assert (primer_trie.search("ACGTA").compare("unknown") == 0);

  // ACGTAA matches primer 1 (+ 2 inserts) and primer 2 (+ 1 insert)
  assert (primer_trie.search("ACGTAA").compare("unknown") == 0);

  // ACGTAAA matches primer 2 (+ 2 inserts)
  assert (primer_trie.search("ACGTAAA").compare("primer2") == 0);

  // ACGTAAA matches primer 2 (+ 2 inserts) + continued string
  assert (primer_trie.search("ACGTAAACCCCC").compare("primer2") == 0);

}


int main(int argc, char **argv) {
  PrimerTrieTest1();
  PrimerTrieTest2();
  PrimerTrieTest3();
  PrimerTrieTest4();
  std::cout << "[primer_trie test] passed" << std::endl;
  return 0;
}
