/*
 * Copyright 2020 Inivata Inc.
 * Copyright 2020 Inivata Ltd.
 */

#include <cassert>
#include <iostream>
#include <sstream>
#include "bam_util.h"
#include "bed_util.h"
#include "entry.h"
#include "fasta_reader.h"

// readTable, writeVariants, baseQualities, mapQuality,closeIndex, destroyTargetIterator, pileupEntry


/** 
 * Non-targeted bam reader test
 */
void nonTargetedTest() {

  // initiate bam reader
  BamUtil bam_util;
  bam_util.open("testdata/test.bam");
  bam_util.initialiseAlignment();
    
  // header
  bam_util.loadHeader();
  assert(bam_util.head->n_targets == 3);

  // Reference1 forward
  bam_util.nextRead();
  assert (bam_util.queryName().compare("Reference1") == 0);
  assert (bam_util.readIsSupplementary() == false);
  assert (bam_util.readIsUsable() == true);
  assert (bam_util.templateIdentifier("chr1") == 0);
  assert (bam_util.referenceName().compare("chr1") == 0);
  assert (bam_util.leftCoordinate() == 23);
  assert (bam_util.rightCoordinate() == 44);
  assert (bam_util.readIsForward() == true);
  assert (bam_util.readIsReverse() == false);
  assert (bam_util.fwdPrefix(12).compare("ACAGAAGAGGCA") == 0);

  // skip reads
  for (int i = 0; i < 8; i ++) {
    bam_util.nextRead();
  }
  
  // Reference1 reverse
  assert (bam_util.queryName().compare("Reference1") == 0);
  assert (bam_util.readIsSupplementary() == false);
  assert (bam_util.readIsUsable() == true);
  assert (bam_util.templateIdentifier("chr1") == 0);
  assert (bam_util.referenceName().compare("chr1") == 0);
  assert (bam_util.leftCoordinate() == 23);
  assert (bam_util.rightCoordinate() == 44);
  assert (bam_util.readIsForward() == false);
  assert (bam_util.readIsReverse() == true);
  assert (bam_util.revPrefix(12).compare("TATATCTGTTTG") == 0);
  assert (bam_util.referenceLength("chr1") == 69);

  // skip reads
  for (int i = 0; i < 8; i ++) {
    bam_util.nextRead();
  }
  
  assert (bam_util.queryName().compare("Unmapped1") == 0);
  assert (bam_util.readIsUsable() == false);

  bam_util.nextRead();
  assert (bam_util.queryName().compare("Unmapped2") == 0);
  assert (bam_util.readIsUsable() == false);

  bam_util.nextRead();
  assert (bam_util.queryName().compare("Secondary1") == 0);
  assert (bam_util.readIsUsable() == false);
  
  bam_util.nextRead();
  assert (bam_util.queryName().compare("Secondary2") == 0);
  assert (bam_util.readIsUsable() == false);
  
  bam_util.nextRead();
  assert (bam_util.queryName().compare("Qcfail1") == 0);
  assert (bam_util.readIsUsable() == false);

  bam_util.nextRead();
  assert (bam_util.queryName().compare("Qcfail2") == 0);
  assert (bam_util.readIsUsable() == false);
  
  // skip supplementary reads
  bam_util.nextRepresentativeRead();
  assert (bam_util.queryName().compare("Paired1") == 0);
  assert (bam_util.readIsUsable() == true);

  bam_util.nextRead();
  assert (bam_util.queryName().compare("Paired2") == 0);
  assert (bam_util.readIsUsable() == true);

  // clean up
  bam_util.destroyAlignment();
  bam_util.close();
  bam_util.closeHeader();
}


/** 
 * Targeted bam reader test
 */
void targetedTest() {

  // initiate bam reader
  BamUtil bam_util;
  bam_util.open("testdata/test.bam");

  // header
  bam_util.loadHeader();
  assert(bam_util.head->n_targets == 3);

  // index
  bam_util.loadIndex();
  
  // initialise alignment and iterator
  bam_util.initialiseAlignment();
  bam_util.initialiseTargetIterator("chr1");

  // iterate through reads (there are 26 reads including Supplementary1 and
  // Supplementary2)
  int ret, n = 0;
  while ((ret = bam_util.nextTargetRead()) > 0) {
      n++;
  }
  assert (n == 26);

  // clean up
  bam_util.destroyAlignment();
  bam_util.close();
  bam_util.closeHeader();
}


/**
 *  Test alignment from testdata/test.bam
 *  1         11        21        31             41        51        61       
 *  |         |         |         |              |         |         |
 *  AAAAACACAGAGACACAGAGACACAGAAGAG****GC*AAACAGATATAGGTAGAGAGACACACAGTTTGAGAG
 *  Reference1            ACAGAAGAG****GC*AAACAGATATA
 *  Reference2            ACAGAAGAG****GC*AAACAGATATA
 *  MismatchC>T           ACAGAAGAG****GC*AAATAGATATA
 *  InsertA               ACAGAAGAG****GCAAAACAGATATA
 *  DeleteG               ACAGAAGA*****GC*AAACAGATATA
 *  InsertACGT            ACAGAAGAGACGTGC*AAACAGATATA
 *  DeleteGG              ACAGAAGA******C*AAACAGATATA
 *  StrandMismatchC>T	    ACAGAAGAG****GC*AAATAGATATA
 *  Reference1            acagaagag****gc*aaacagatata
 *  Reference2            acagaagag****gc*aaacagatata
 *  InsertA               acagaagag****gcaaaacagatata
 *  DeleteG               acagaaga*****gc*aaacagatata
 *  InsertACGT            acagaagagacgtgc*aaacagatata
 *  DeleteGG              acagaaga******c*aaacagatata
 *  StrandMismatchC>T     acagaagag****gc*aaacagatata
 
 *  the following reads should be excluded from the pileup as they are not
 *  usable (unmapped, secondary, qc fail, supplementary, or not proper pair)
 *  Secondary1            ACAGAAGAG****GC*AAACAGATATA
 *  Secondary2            ACAGAAGAG****GC*AAACAGATATA
 *  Qcfail1               ACAGAAGAG****GC*AAACAGATATA
 *  Qcfail2               ACAGAAGAG****GC*AAACAGATATA
 *  Supplementary1        ACAGAAGAG****GC*AAACAGATATA
 *  Supplementary1        ACAGAAGAG****GC*AAACAGATATA
 *  Paired1               ACAGAAGAG****GC*AAACAGATATA
 *  Paired2               ACAGAAGAG****GC*AAACAGATATA
 */

void indexReadsTest() {
  // expected bases
  std::map<std::string, std::string> expected_bases;
  expected_bases["Reference1,0,23"] = "A";
  expected_bases["Reference1,0,24"] = "C";
  expected_bases["Reference1,0,25"] = "A";
  expected_bases["Reference1,0,26"] = "G";
  expected_bases["Reference1,0,27"] = "A";
  expected_bases["Reference1,0,28"] = "A";
  expected_bases["Reference1,0,29"] = "G";
  expected_bases["Reference1,0,30"] = "A";
  expected_bases["Reference1,0,31"] = "G";
  expected_bases["Reference1,0,32"] = "G";
  expected_bases["Reference1,0,33"] = "C";
  expected_bases["Reference1,0,34"] = "A";
  expected_bases["Reference1,0,35"] = "A";
  expected_bases["Reference1,0,36"] = "A";
  expected_bases["Reference1,0,37"] = "C";
  expected_bases["Reference1,0,38"] = "A";
  expected_bases["Reference1,0,39"] = "G";
  expected_bases["Reference1,0,40"] = "A";
  expected_bases["Reference1,0,41"] = "T";
  expected_bases["Reference1,0,42"] = "A";
  expected_bases["Reference1,0,43"] = "T";
  expected_bases["Reference1,0,44"] = "A";
  expected_bases["Reference1,1,23"] = "A";
  expected_bases["Reference1,1,24"] = "C";
  expected_bases["Reference1,1,25"] = "A";
  expected_bases["Reference1,1,26"] = "G";
  expected_bases["Reference1,1,27"] = "A";
  expected_bases["Reference1,1,28"] = "A";
  expected_bases["Reference1,1,29"] = "G";
  expected_bases["Reference1,1,30"] = "A";
  expected_bases["Reference1,1,31"] = "G";
  expected_bases["Reference1,1,32"] = "G";
  expected_bases["Reference1,1,33"] = "C";
  expected_bases["Reference1,1,34"] = "A";
  expected_bases["Reference1,1,35"] = "A";
  expected_bases["Reference1,1,36"] = "A";
  expected_bases["Reference1,1,37"] = "C";
  expected_bases["Reference1,1,38"] = "A";
  expected_bases["Reference1,1,39"] = "G";
  expected_bases["Reference1,1,40"] = "A";
  expected_bases["Reference1,1,41"] = "T";
  expected_bases["Reference1,1,42"] = "A";
  expected_bases["Reference1,1,43"] = "T";
  expected_bases["Reference1,1,44"] = "A";
  expected_bases["Reference2,0,23"] = "A";
  expected_bases["Reference2,0,24"] = "C";
  expected_bases["Reference2,0,25"] = "A";
  expected_bases["Reference2,0,26"] = "G";
  expected_bases["Reference2,0,27"] = "A";
  expected_bases["Reference2,0,28"] = "A";
  expected_bases["Reference2,0,29"] = "G";
  expected_bases["Reference2,0,30"] = "A";
  expected_bases["Reference2,0,31"] = "G";
  expected_bases["Reference2,0,32"] = "G";
  expected_bases["Reference2,0,33"] = "C";
  expected_bases["Reference2,0,34"] = "A";
  expected_bases["Reference2,0,35"] = "A";
  expected_bases["Reference2,0,36"] = "A";
  expected_bases["Reference2,0,37"] = "C";
  expected_bases["Reference2,0,38"] = "A";
  expected_bases["Reference2,0,39"] = "G";
  expected_bases["Reference2,0,40"] = "A";
  expected_bases["Reference2,0,41"] = "T";
  expected_bases["Reference2,0,42"] = "A";
  expected_bases["Reference2,0,43"] = "T";
  expected_bases["Reference2,0,44"] = "A";
  expected_bases["Reference2,1,23"] = "A";
  expected_bases["Reference2,1,24"] = "C";
  expected_bases["Reference2,1,25"] = "A";
  expected_bases["Reference2,1,26"] = "G";
  expected_bases["Reference2,1,27"] = "A";
  expected_bases["Reference2,1,28"] = "A";
  expected_bases["Reference2,1,29"] = "G";
  expected_bases["Reference2,1,30"] = "A";
  expected_bases["Reference2,1,31"] = "G";
  expected_bases["Reference2,1,32"] = "G";
  expected_bases["Reference2,1,33"] = "C";
  expected_bases["Reference2,1,34"] = "A";
  expected_bases["Reference2,1,35"] = "A";
  expected_bases["Reference2,1,36"] = "A";
  expected_bases["Reference2,1,37"] = "C";
  expected_bases["Reference2,1,38"] = "A";
  expected_bases["Reference2,1,39"] = "G";
  expected_bases["Reference2,1,40"] = "A";
  expected_bases["Reference2,1,41"] = "T";
  expected_bases["Reference2,1,42"] = "A";
  expected_bases["Reference2,1,43"] = "T";
  expected_bases["Reference2,1,44"] = "A";
  expected_bases["MismatchC>T,0,23"] = "A";
  expected_bases["MismatchC>T,0,24"] = "C";
  expected_bases["MismatchC>T,0,25"] = "A";
  expected_bases["MismatchC>T,0,26"] = "G";
  expected_bases["MismatchC>T,0,27"] = "A";
  expected_bases["MismatchC>T,0,28"] = "A";
  expected_bases["MismatchC>T,0,29"] = "G";
  expected_bases["MismatchC>T,0,30"] = "A";
  expected_bases["MismatchC>T,0,31"] = "G";
  expected_bases["MismatchC>T,0,32"] = "G";
  expected_bases["MismatchC>T,0,33"] = "C";
  expected_bases["MismatchC>T,0,34"] = "A";
  expected_bases["MismatchC>T,0,35"] = "A";
  expected_bases["MismatchC>T,0,36"] = "A";
  expected_bases["MismatchC>T,0,37"] = "T";
  expected_bases["MismatchC>T,0,38"] = "A";
  expected_bases["MismatchC>T,0,39"] = "G";
  expected_bases["MismatchC>T,0,40"] = "A";
  expected_bases["MismatchC>T,0,41"] = "T";
  expected_bases["MismatchC>T,0,42"] = "A";
  expected_bases["MismatchC>T,0,43"] = "T";
  expected_bases["MismatchC>T,0,44"] = "A";
  expected_bases["MismatchC>T,1,23"] = "A";
  expected_bases["MismatchC>T,1,24"] = "C";
  expected_bases["MismatchC>T,1,25"] = "A";
  expected_bases["MismatchC>T,1,26"] = "G";
  expected_bases["MismatchC>T,1,27"] = "A";
  expected_bases["MismatchC>T,1,28"] = "A";
  expected_bases["MismatchC>T,1,29"] = "G";
  expected_bases["MismatchC>T,1,30"] = "A";
  expected_bases["MismatchC>T,1,31"] = "G";
  expected_bases["MismatchC>T,1,32"] = "G";
  expected_bases["MismatchC>T,1,33"] = "C";
  expected_bases["MismatchC>T,1,34"] = "A";
  expected_bases["MismatchC>T,1,35"] = "A";
  expected_bases["MismatchC>T,1,36"] = "A";
  expected_bases["MismatchC>T,1,37"] = "T";
  expected_bases["MismatchC>T,1,38"] = "A";
  expected_bases["MismatchC>T,1,39"] = "G";
  expected_bases["MismatchC>T,1,40"] = "A";
  expected_bases["MismatchC>T,1,41"] = "T";
  expected_bases["MismatchC>T,1,42"] = "A";
  expected_bases["MismatchC>T,1,43"] = "T";
  expected_bases["MismatchC>T,1,44"] = "A";
  expected_bases["InsertA,0,23"] = "A";
  expected_bases["InsertA,0,24"] = "C";
  expected_bases["InsertA,0,25"] = "A";
  expected_bases["InsertA,0,26"] = "G";
  expected_bases["InsertA,0,27"] = "A";
  expected_bases["InsertA,0,28"] = "A";
  expected_bases["InsertA,0,29"] = "G";
  expected_bases["InsertA,0,30"] = "A";
  expected_bases["InsertA,0,31"] = "G";
  expected_bases["InsertA,0,32"] = "G";
  expected_bases["InsertA,0,33"] = "C+1A";
  expected_bases["InsertA,0,34"] = "A";
  expected_bases["InsertA,0,35"] = "A";
  expected_bases["InsertA,0,36"] = "A";
  expected_bases["InsertA,0,37"] = "C";
  expected_bases["InsertA,0,38"] = "A";
  expected_bases["InsertA,0,39"] = "G";
  expected_bases["InsertA,0,40"] = "A";
  expected_bases["InsertA,0,41"] = "T";
  expected_bases["InsertA,0,42"] = "A";
  expected_bases["InsertA,0,43"] = "T";
  expected_bases["InsertA,0,44"] = "A";
  expected_bases["InsertA,1,23"] = "A";
  expected_bases["InsertA,1,24"] = "C";
  expected_bases["InsertA,1,25"] = "A";
  expected_bases["InsertA,1,26"] = "G";
  expected_bases["InsertA,1,27"] = "A";
  expected_bases["InsertA,1,28"] = "A";
  expected_bases["InsertA,1,29"] = "G";
  expected_bases["InsertA,1,30"] = "A";
  expected_bases["InsertA,1,31"] = "G";
  expected_bases["InsertA,1,32"] = "G";
  expected_bases["InsertA,1,33"] = "C+1A";
  expected_bases["InsertA,1,34"] = "A";
  expected_bases["InsertA,1,35"] = "A";
  expected_bases["InsertA,1,36"] = "A";
  expected_bases["InsertA,1,37"] = "C";
  expected_bases["InsertA,1,38"] = "A";
  expected_bases["InsertA,1,39"] = "G";
  expected_bases["InsertA,1,40"] = "A";
  expected_bases["InsertA,1,41"] = "T";
  expected_bases["InsertA,1,42"] = "A";
  expected_bases["InsertA,1,43"] = "T";
  expected_bases["InsertA,1,44"] = "A";
  expected_bases["DeleteG,0,23"] = "A";
  expected_bases["DeleteG,0,24"] = "C";
  expected_bases["DeleteG,0,25"] = "A";
  expected_bases["DeleteG,0,26"] = "G";
  expected_bases["DeleteG,0,27"] = "A";
  expected_bases["DeleteG,0,28"] = "A";
  expected_bases["DeleteG,0,29"] = "G";
  expected_bases["DeleteG,0,30"] = "A-1G";
  expected_bases["DeleteG,0,31"] = "*";
  expected_bases["DeleteG,0,32"] = "G";
  expected_bases["DeleteG,0,33"] = "C";
  expected_bases["DeleteG,0,34"] = "A";
  expected_bases["DeleteG,0,35"] = "A";
  expected_bases["DeleteG,0,36"] = "A";
  expected_bases["DeleteG,0,37"] = "C";
  expected_bases["DeleteG,0,38"] = "A";
  expected_bases["DeleteG,0,39"] = "G";
  expected_bases["DeleteG,0,40"] = "A";
  expected_bases["DeleteG,0,41"] = "T";
  expected_bases["DeleteG,0,42"] = "A";
  expected_bases["DeleteG,0,43"] = "T";
  expected_bases["DeleteG,0,44"] = "A";
  expected_bases["DeleteG,1,23"] = "A";
  expected_bases["DeleteG,1,24"] = "C";
  expected_bases["DeleteG,1,25"] = "A";
  expected_bases["DeleteG,1,26"] = "G";
  expected_bases["DeleteG,1,27"] = "A";
  expected_bases["DeleteG,1,28"] = "A";
  expected_bases["DeleteG,1,29"] = "G";
  expected_bases["DeleteG,1,30"] = "A-1G";
  expected_bases["DeleteG,1,31"] = "*";
  expected_bases["DeleteG,1,32"] = "G";
  expected_bases["DeleteG,1,33"] = "C";
  expected_bases["DeleteG,1,34"] = "A";
  expected_bases["DeleteG,1,35"] = "A";
  expected_bases["DeleteG,1,36"] = "A";
  expected_bases["DeleteG,1,37"] = "C";
  expected_bases["DeleteG,1,38"] = "A";
  expected_bases["DeleteG,1,39"] = "G";
  expected_bases["DeleteG,1,40"] = "A";
  expected_bases["DeleteG,1,41"] = "T";
  expected_bases["DeleteG,1,42"] = "A";
  expected_bases["DeleteG,1,43"] = "T";
  expected_bases["DeleteG,1,44"] = "A";
  expected_bases["InsertACGT,0,23"] = "A";
  expected_bases["InsertACGT,0,24"] = "C";
  expected_bases["InsertACGT,0,25"] = "A";
  expected_bases["InsertACGT,0,26"] = "G";
  expected_bases["InsertACGT,0,27"] = "A";
  expected_bases["InsertACGT,0,28"] = "A";
  expected_bases["InsertACGT,0,29"] = "G";
  expected_bases["InsertACGT,0,30"] = "A";
  expected_bases["InsertACGT,0,31"] = "G+4ACGT";
  expected_bases["InsertACGT,0,32"] = "G";
  expected_bases["InsertACGT,0,33"] = "C";
  expected_bases["InsertACGT,0,34"] = "A";
  expected_bases["InsertACGT,0,35"] = "A";
  expected_bases["InsertACGT,0,36"] = "A";
  expected_bases["InsertACGT,0,37"] = "C";
  expected_bases["InsertACGT,0,38"] = "A";
  expected_bases["InsertACGT,0,39"] = "G";
  expected_bases["InsertACGT,0,40"] = "A";
  expected_bases["InsertACGT,0,41"] = "T";
  expected_bases["InsertACGT,0,42"] = "A";
  expected_bases["InsertACGT,0,43"] = "T";
  expected_bases["InsertACGT,0,44"] = "A";
  expected_bases["InsertACGT,1,23"] = "A";
  expected_bases["InsertACGT,1,24"] = "C";
  expected_bases["InsertACGT,1,25"] = "A";
  expected_bases["InsertACGT,1,26"] = "G";
  expected_bases["InsertACGT,1,27"] = "A";
  expected_bases["InsertACGT,1,28"] = "A";
  expected_bases["InsertACGT,1,29"] = "G";
  expected_bases["InsertACGT,1,30"] = "A";
  expected_bases["InsertACGT,1,31"] = "G+4ACGT";
  expected_bases["InsertACGT,1,32"] = "G";
  expected_bases["InsertACGT,1,33"] = "C";
  expected_bases["InsertACGT,1,34"] = "A";
  expected_bases["InsertACGT,1,35"] = "A";
  expected_bases["InsertACGT,1,36"] = "A";
  expected_bases["InsertACGT,1,37"] = "C";
  expected_bases["InsertACGT,1,38"] = "A";
  expected_bases["InsertACGT,1,39"] = "G";
  expected_bases["InsertACGT,1,40"] = "A";
  expected_bases["InsertACGT,1,41"] = "T";
  expected_bases["InsertACGT,1,42"] = "A";
  expected_bases["InsertACGT,1,43"] = "T";
  expected_bases["InsertACGT,1,44"] = "A";
  expected_bases["DeleteGG,0,23"] = "A";
  expected_bases["DeleteGG,0,24"] = "C";
  expected_bases["DeleteGG,0,25"] = "A";
  expected_bases["DeleteGG,0,26"] = "G";
  expected_bases["DeleteGG,0,27"] = "A";
  expected_bases["DeleteGG,0,28"] = "A";
  expected_bases["DeleteGG,0,29"] = "G";
  expected_bases["DeleteGG,0,30"] = "A-2GG";
  expected_bases["DeleteGG,0,31"] = "*";
  expected_bases["DeleteGG,0,32"] = "*";
  expected_bases["DeleteGG,0,33"] = "C";
  expected_bases["DeleteGG,0,34"] = "A";
  expected_bases["DeleteGG,0,35"] = "A";
  expected_bases["DeleteGG,0,36"] = "A";
  expected_bases["DeleteGG,0,37"] = "C";
  expected_bases["DeleteGG,0,38"] = "A";
  expected_bases["DeleteGG,0,39"] = "G";
  expected_bases["DeleteGG,0,40"] = "A";
  expected_bases["DeleteGG,0,41"] = "T";
  expected_bases["DeleteGG,0,42"] = "A";
  expected_bases["DeleteGG,0,43"] = "T";
  expected_bases["DeleteGG,0,44"] = "A";
  expected_bases["DeleteGG,1,23"] = "A";
  expected_bases["DeleteGG,1,24"] = "C";
  expected_bases["DeleteGG,1,25"] = "A";
  expected_bases["DeleteGG,1,26"] = "G";
  expected_bases["DeleteGG,1,27"] = "A";
  expected_bases["DeleteGG,1,28"] = "A";
  expected_bases["DeleteGG,1,29"] = "G";
  expected_bases["DeleteGG,1,30"] = "A-2GG";
  expected_bases["DeleteGG,1,31"] = "*";
  expected_bases["DeleteGG,1,32"] = "*";
  expected_bases["DeleteGG,1,33"] = "C";
  expected_bases["DeleteGG,1,34"] = "A";
  expected_bases["DeleteGG,1,35"] = "A";
  expected_bases["DeleteGG,1,36"] = "A";
  expected_bases["DeleteGG,1,37"] = "C";
  expected_bases["DeleteGG,1,38"] = "A";
  expected_bases["DeleteGG,1,39"] = "G";
  expected_bases["DeleteGG,1,40"] = "A";
  expected_bases["DeleteGG,1,41"] = "T";
  expected_bases["DeleteGG,1,42"] = "A";
  expected_bases["DeleteGG,1,43"] = "T";
  expected_bases["DeleteGG,1,44"] = "A";
  expected_bases["StrandMismatchC>T,0,23"] = "A";
  expected_bases["StrandMismatchC>T,0,24"] = "C";
  expected_bases["StrandMismatchC>T,0,25"] = "A";
  expected_bases["StrandMismatchC>T,0,26"] = "G";
  expected_bases["StrandMismatchC>T,0,27"] = "A";
  expected_bases["StrandMismatchC>T,0,28"] = "A";
  expected_bases["StrandMismatchC>T,0,29"] = "G";
  expected_bases["StrandMismatchC>T,0,30"] = "A";
  expected_bases["StrandMismatchC>T,0,31"] = "G";
  expected_bases["StrandMismatchC>T,0,32"] = "G";
  expected_bases["StrandMismatchC>T,0,33"] = "C";
  expected_bases["StrandMismatchC>T,0,34"] = "A";
  expected_bases["StrandMismatchC>T,0,35"] = "A";
  expected_bases["StrandMismatchC>T,0,36"] = "A";
  expected_bases["StrandMismatchC>T,0,37"] = "T";
  expected_bases["StrandMismatchC>T,0,38"] = "A";
  expected_bases["StrandMismatchC>T,0,39"] = "G";
  expected_bases["StrandMismatchC>T,0,40"] = "A";
  expected_bases["StrandMismatchC>T,0,41"] = "T";
  expected_bases["StrandMismatchC>T,0,42"] = "A";
  expected_bases["StrandMismatchC>T,0,43"] = "T";
  expected_bases["StrandMismatchC>T,0,44"] = "A";
  expected_bases["StrandMismatchC>T,1,23"] = "A";
  expected_bases["StrandMismatchC>T,1,24"] = "C";
  expected_bases["StrandMismatchC>T,1,25"] = "A";
  expected_bases["StrandMismatchC>T,1,26"] = "G";
  expected_bases["StrandMismatchC>T,1,27"] = "A";
  expected_bases["StrandMismatchC>T,1,28"] = "A";
  expected_bases["StrandMismatchC>T,1,29"] = "G";
  expected_bases["StrandMismatchC>T,1,30"] = "A";
  expected_bases["StrandMismatchC>T,1,31"] = "G";
  expected_bases["StrandMismatchC>T,1,32"] = "G";
  expected_bases["StrandMismatchC>T,1,33"] = "C";
  expected_bases["StrandMismatchC>T,1,34"] = "A";
  expected_bases["StrandMismatchC>T,1,35"] = "A";
  expected_bases["StrandMismatchC>T,1,36"] = "A";
  expected_bases["StrandMismatchC>T,1,37"] = "C";
  expected_bases["StrandMismatchC>T,1,38"] = "A";
  expected_bases["StrandMismatchC>T,1,39"] = "G";
  expected_bases["StrandMismatchC>T,1,40"] = "A";
  expected_bases["StrandMismatchC>T,1,41"] = "T";
  expected_bases["StrandMismatchC>T,1,42"] = "A";
  expected_bases["StrandMismatchC>T,1,43"] = "T";
  expected_bases["StrandMismatchC>T,1,44"] = "A";

  // expected qualities
  std::map<std::string, std::map<int, int>> expected_quals;
  expected_quals["Reference1,0,23"] = { { 15 , 1 } };
  expected_quals["Reference1,0,24"] = { { 15 , 1 } };
  expected_quals["Reference1,0,25"] = { { 15 , 1 } };
  expected_quals["Reference1,0,26"] = { { 15 , 1 } };
  expected_quals["Reference1,0,27"] = { { 15 , 1 } };
  expected_quals["Reference1,0,28"] = { { 15 , 1 } };
  expected_quals["Reference1,0,29"] = { { 15 , 1 } };
  expected_quals["Reference1,0,30"] = { { 15 , 1 } };
  expected_quals["Reference1,0,31"] = { { 15 , 1 } };
  expected_quals["Reference1,0,32"] = { { 15 , 1 } };
  expected_quals["Reference1,0,33"] = { { 15 , 1 } };
  expected_quals["Reference1,0,34"] = { { 15 , 1 } };
  expected_quals["Reference1,0,35"] = { { 15 , 1 } };
  expected_quals["Reference1,0,36"] = { { 15 , 1 } };
  expected_quals["Reference1,0,37"] = { { 15 , 1 } };
  expected_quals["Reference1,0,38"] = { { 15 , 1 } };
  expected_quals["Reference1,0,39"] = { { 15 , 1 } };
  expected_quals["Reference1,0,40"] = { { 15 , 1 } };
  expected_quals["Reference1,0,41"] = { { 15 , 1 } };
  expected_quals["Reference1,0,42"] = { { 15 , 1 } };
  expected_quals["Reference1,0,43"] = { { 15 , 1 } };
  expected_quals["Reference1,0,44"] = { { 15 , 1 } };
  expected_quals["Reference1,1,23"] = { { 40 , 1 } };
  expected_quals["Reference1,1,24"] = { { 40 , 1 } };
  expected_quals["Reference1,1,25"] = { { 40 , 1 } };
  expected_quals["Reference1,1,26"] = { { 40 , 1 } };
  expected_quals["Reference1,1,27"] = { { 40 , 1 } };
  expected_quals["Reference1,1,28"] = { { 40 , 1 } };
  expected_quals["Reference1,1,29"] = { { 40 , 1 } };
  expected_quals["Reference1,1,30"] = { { 40 , 1 } };
  expected_quals["Reference1,1,31"] = { { 40 , 1 } };
  expected_quals["Reference1,1,32"] = { { 40 , 1 } };
  expected_quals["Reference1,1,33"] = { { 40 , 1 } };
  expected_quals["Reference1,1,34"] = { { 40 , 1 } };
  expected_quals["Reference1,1,35"] = { { 40 , 1 } };
  expected_quals["Reference1,1,36"] = { { 40 , 1 } };
  expected_quals["Reference1,1,37"] = { { 40 , 1 } };
  expected_quals["Reference1,1,38"] = { { 40 , 1 } };
  expected_quals["Reference1,1,39"] = { { 40 , 1 } };
  expected_quals["Reference1,1,40"] = { { 40 , 1 } };
  expected_quals["Reference1,1,41"] = { { 40 , 1 } };
  expected_quals["Reference1,1,42"] = { { 40 , 1 } };
  expected_quals["Reference1,1,43"] = { { 40 , 1 } };
  expected_quals["Reference1,1,44"] = { { 40 , 1 } };
  expected_quals["Reference2,0,23"] = { { 39 , 1 } };
  expected_quals["Reference2,0,24"] = { { 39 , 1 } };
  expected_quals["Reference2,0,25"] = { { 39 , 1 } };
  expected_quals["Reference2,0,26"] = { { 39 , 1 } };
  expected_quals["Reference2,0,27"] = { { 39 , 1 } };
  expected_quals["Reference2,0,28"] = { { 39 , 1 } };
  expected_quals["Reference2,0,29"] = { { 39 , 1 } };
  expected_quals["Reference2,0,30"] = { { 39 , 1 } };
  expected_quals["Reference2,0,31"] = { { 39 , 1 } };
  expected_quals["Reference2,0,32"] = { { 39 , 1 } };
  expected_quals["Reference2,0,33"] = { { 39 , 1 } };
  expected_quals["Reference2,0,34"] = { { 39 , 1 } };
  expected_quals["Reference2,0,35"] = { { 39 , 1 } };
  expected_quals["Reference2,0,36"] = { { 39 , 1 } };
  expected_quals["Reference2,0,37"] = { { 39 , 1 } };
  expected_quals["Reference2,0,38"] = { { 39 , 1 } };
  expected_quals["Reference2,0,39"] = { { 39 , 1 } };
  expected_quals["Reference2,0,40"] = { { 39 , 1 } };
  expected_quals["Reference2,0,41"] = { { 39 , 1 } };
  expected_quals["Reference2,0,42"] = { { 39 , 1 } };
  expected_quals["Reference2,0,43"] = { { 39 , 1 } };
  expected_quals["Reference2,0,44"] = { { 39 , 1 } };
  expected_quals["Reference2,1,23"] = { {16 , 1 } };
  expected_quals["Reference2,1,24"] = { {16 , 1 } };
  expected_quals["Reference2,1,25"] = { {16 , 1 } };
  expected_quals["Reference2,1,26"] = { {16 , 1 } };
  expected_quals["Reference2,1,27"] = { {16 , 1 } };
  expected_quals["Reference2,1,28"] = { {16 , 1 } };
  expected_quals["Reference2,1,29"] = { {16 , 1 } };
  expected_quals["Reference2,1,30"] = { {16 , 1 } };
  expected_quals["Reference2,1,31"] = { {16 , 1 } };
  expected_quals["Reference2,1,32"] = { {16 , 1 } };
  expected_quals["Reference2,1,33"] = { {16 , 1 } };
  expected_quals["Reference2,1,34"] = { {16 , 1 } };
  expected_quals["Reference2,1,35"] = { {16 , 1 } };
  expected_quals["Reference2,1,36"] = { {16 , 1 } };
  expected_quals["Reference2,1,37"] = { {16 , 1 } };
  expected_quals["Reference2,1,38"] = { {16 , 1 } };
  expected_quals["Reference2,1,39"] = { {16 , 1 } };
  expected_quals["Reference2,1,40"] = { {16 , 1 } };
  expected_quals["Reference2,1,41"] = { {16 , 1 } };
  expected_quals["Reference2,1,42"] = { {16 , 1 } };
  expected_quals["Reference2,1,43"] = { {16 , 1 } };
  expected_quals["Reference2,1,44"] = { {16 , 1 } };
  expected_quals["MismatchC>T,0,23"] = { { 15 , 1 } };
  expected_quals["MismatchC>T,0,24"] = { { 15 , 1 } };
  expected_quals["MismatchC>T,0,25"] = { { 15 , 1 } };
  expected_quals["MismatchC>T,0,26"] = { { 15 , 1 } };
  expected_quals["MismatchC>T,0,27"] = { { 15 , 1 } };
  expected_quals["MismatchC>T,0,28"] = { { 15 , 1 } };
  expected_quals["MismatchC>T,0,29"] = { { 15 , 1 } };
  expected_quals["MismatchC>T,0,30"] = { { 15 , 1 } };
  expected_quals["MismatchC>T,0,31"] = { { 15 , 1 } };
  expected_quals["MismatchC>T,0,32"] = { { 15 , 1 } };
  expected_quals["MismatchC>T,0,33"] = { { 15 , 1 } };
  expected_quals["MismatchC>T,0,34"] = { { 15 , 1 } };
  expected_quals["MismatchC>T,0,35"] = { { 15 , 1 } };
  expected_quals["MismatchC>T,0,36"] = { { 15 , 1 } };
  expected_quals["MismatchC>T,0,37"] = { { 15 , 1 } };
  expected_quals["MismatchC>T,0,38"] = { { 15 , 1 } };
  expected_quals["MismatchC>T,0,39"] = { { 15 , 1 } };
  expected_quals["MismatchC>T,0,40"] = { { 15 , 1 } };
  expected_quals["MismatchC>T,0,41"] = { { 15 , 1 } };
  expected_quals["MismatchC>T,0,42"] = { { 15 , 1 } };
  expected_quals["MismatchC>T,0,43"] = { { 15 , 1 } };
  expected_quals["MismatchC>T,0,44"] = { { 15 , 1 } };
  expected_quals["MismatchC>T,1,23"] = { { 40 , 1 } };
  expected_quals["MismatchC>T,1,24"] = { { 40 , 1 } };
  expected_quals["MismatchC>T,1,25"] = { { 40 , 1 } };
  expected_quals["MismatchC>T,1,26"] = { { 40 , 1 } };
  expected_quals["MismatchC>T,1,27"] = { { 40 , 1 } };
  expected_quals["MismatchC>T,1,28"] = { { 40 , 1 } };
  expected_quals["MismatchC>T,1,29"] = { { 40 , 1 } };
  expected_quals["MismatchC>T,1,30"] = { { 40 , 1 } };
  expected_quals["MismatchC>T,1,31"] = { { 40 , 1 } };
  expected_quals["MismatchC>T,1,32"] = { { 40 , 1 } };
  expected_quals["MismatchC>T,1,33"] = { { 40 , 1 } };
  expected_quals["MismatchC>T,1,34"] = { { 40 , 1 } };
  expected_quals["MismatchC>T,1,35"] = { { 40 , 1 } };
  expected_quals["MismatchC>T,1,36"] = { { 40 , 1 } };
  expected_quals["MismatchC>T,1,37"] = { { 40 , 1 } };
  expected_quals["MismatchC>T,1,38"] = { { 40 , 1 } };
  expected_quals["MismatchC>T,1,39"] = { { 40 , 1 } };
  expected_quals["MismatchC>T,1,40"] = { { 40 , 1 } };
  expected_quals["MismatchC>T,1,41"] = { { 40 , 1 } };
  expected_quals["MismatchC>T,1,42"] = { { 40 , 1 } };
  expected_quals["MismatchC>T,1,43"] = { { 40 , 1 } };
  expected_quals["MismatchC>T,1,44"] = { { 40 , 1 } };
  expected_quals["InsertA,0,23"] = { { 39 , 1 } };
  expected_quals["InsertA,0,24"] = { { 39 , 1 } };
  expected_quals["InsertA,0,25"] = { { 39 , 1 } };
  expected_quals["InsertA,0,26"] = { { 39 , 1 } };
  expected_quals["InsertA,0,27"] = { { 39 , 1 } };
  expected_quals["InsertA,0,28"] = { { 39 , 1 } };
  expected_quals["InsertA,0,29"] = { { 39 , 1 } };
  expected_quals["InsertA,0,30"] = { { 39 , 1 } };
  expected_quals["InsertA,0,31"] = { { 39 , 1 } };
  expected_quals["InsertA,0,32"] = { { 39 , 1 } };
  expected_quals["InsertA,0,33"] = { { 39 , 2 } };
  expected_quals["InsertA,0,34"] = { { 39 , 1 } };
  expected_quals["InsertA,0,35"] = { { 39 , 1 } };
  expected_quals["InsertA,0,36"] = { { 39 , 1 } };
  expected_quals["InsertA,0,37"] = { { 39 , 1 } };
  expected_quals["InsertA,0,38"] = { { 39 , 1 } };
  expected_quals["InsertA,0,39"] = { { 39 , 1 } };
  expected_quals["InsertA,0,40"] = { { 39 , 1 } };
  expected_quals["InsertA,0,41"] = { { 39 , 1 } };
  expected_quals["InsertA,0,42"] = { { 39 , 1 } };
  expected_quals["InsertA,0,43"] = { { 39 , 1 } };
  expected_quals["InsertA,0,44"] = { { 39 , 1 } };
  expected_quals["InsertA,1,23"] = { {16 , 1 } };
  expected_quals["InsertA,1,24"] = { {16 , 1 } };
  expected_quals["InsertA,1,25"] = { {16 , 1 } };
  expected_quals["InsertA,1,26"] = { {16 , 1 } };
  expected_quals["InsertA,1,27"] = { {16 , 1 } };
  expected_quals["InsertA,1,28"] = { {16 , 1 } };
  expected_quals["InsertA,1,29"] = { {16 , 1 } };
  expected_quals["InsertA,1,30"] = { {16 , 1 } };
  expected_quals["InsertA,1,31"] = { {16 , 1 } };
  expected_quals["InsertA,1,32"] = { {16 , 1 } };
  expected_quals["InsertA,1,33"] = { {16 , 2 } };
  expected_quals["InsertA,1,34"] = { {16 , 1 } };
  expected_quals["InsertA,1,35"] = { {16 , 1 } };
  expected_quals["InsertA,1,36"] = { {16 , 1 } };
  expected_quals["InsertA,1,37"] = { {16 , 1 } };
  expected_quals["InsertA,1,38"] = { {16 , 1 } };
  expected_quals["InsertA,1,39"] = { {16 , 1 } };
  expected_quals["InsertA,1,40"] = { {16 , 1 } };
  expected_quals["InsertA,1,41"] = { {16 , 1 } };
  expected_quals["InsertA,1,42"] = { {16 , 1 } };
  expected_quals["InsertA,1,43"] = { {16 , 1 } };
  expected_quals["InsertA,1,44"] = { {16 , 1 } };
  expected_quals["DeleteG,0,23"] = { { 15 , 1 } };
  expected_quals["DeleteG,0,24"] = { { 15 , 1 } };
  expected_quals["DeleteG,0,25"] = { { 15 , 1 } };
  expected_quals["DeleteG,0,26"] = { { 15 , 1 } };
  expected_quals["DeleteG,0,27"] = { { 15 , 1 } };
  expected_quals["DeleteG,0,28"] = { { 15 , 1 } };
  expected_quals["DeleteG,0,29"] = { { 15 , 1 } };
  expected_quals["DeleteG,0,30"] = { { 0 , 1 }, { 15 , 1 } };
  expected_quals["DeleteG,0,32"] = { { 15 , 1 } };
  expected_quals["DeleteG,0,33"] = { { 15 , 1 } };
  expected_quals["DeleteG,0,34"] = { { 15 , 1 } };
  expected_quals["DeleteG,0,35"] = { { 15 , 1 } };
  expected_quals["DeleteG,0,36"] = { { 15 , 1 } };
  expected_quals["DeleteG,0,37"] = { { 15 , 1 } };
  expected_quals["DeleteG,0,38"] = { { 15 , 1 } };
  expected_quals["DeleteG,0,39"] = { { 15 , 1 } };
  expected_quals["DeleteG,0,40"] = { { 15 , 1 } };
  expected_quals["DeleteG,0,41"] = { { 15 , 1 } };
  expected_quals["DeleteG,0,42"] = { { 15 , 1 } };
  expected_quals["DeleteG,0,43"] = { { 15 , 1 } };
  expected_quals["DeleteG,0,44"] = { { 15 , 1 } };
  expected_quals["DeleteG,1,23"] = { { 40 , 1 } };
  expected_quals["DeleteG,1,24"] = { { 40 , 1 } };
  expected_quals["DeleteG,1,25"] = { { 40 , 1 } };
  expected_quals["DeleteG,1,26"] = { { 40 , 1 } };
  expected_quals["DeleteG,1,27"] = { { 40 , 1 } };
  expected_quals["DeleteG,1,28"] = { { 40 , 1 } };
  expected_quals["DeleteG,1,29"] = { { 40 , 1 } };
  expected_quals["DeleteG,1,30"] = { { 0 , 1 }, { 40 , 1 } };
  expected_quals["DeleteG,1,32"] = { { 40 , 1 } };
  expected_quals["DeleteG,1,33"] = { { 40 , 1 } };
  expected_quals["DeleteG,1,34"] = { { 40 , 1 } };
  expected_quals["DeleteG,1,35"] = { { 40 , 1 } };
  expected_quals["DeleteG,1,36"] = { { 40 , 1 } };
  expected_quals["DeleteG,1,37"] = { { 40 , 1 } };
  expected_quals["DeleteG,1,38"] = { { 40 , 1 } };
  expected_quals["DeleteG,1,39"] = { { 40 , 1 } };
  expected_quals["DeleteG,1,40"] = { { 40 , 1 } };
  expected_quals["DeleteG,1,41"] = { { 40 , 1 } };
  expected_quals["DeleteG,1,42"] = { { 40 , 1 } };
  expected_quals["DeleteG,1,43"] = { { 40 , 1 } };
  expected_quals["DeleteG,1,44"] = { { 40 , 1 } };
  expected_quals["InsertACGT,0,23"] = { { 39 , 1 } };
  expected_quals["InsertACGT,0,24"] = { { 39 , 1 } };
  expected_quals["InsertACGT,0,25"] = { { 39 , 1 } };
  expected_quals["InsertACGT,0,26"] = { { 39 , 1 } };
  expected_quals["InsertACGT,0,27"] = { { 39 , 1 } };
  expected_quals["InsertACGT,0,28"] = { { 39 , 1 } };
  expected_quals["InsertACGT,0,29"] = { { 39 , 1 } };
  expected_quals["InsertACGT,0,30"] = { { 39 , 1 } };
  expected_quals["InsertACGT,0,31"] = { { 39 , 5 } };
  expected_quals["InsertACGT,0,32"] = { { 39 , 1 } };
  expected_quals["InsertACGT,0,33"] = { { 39 , 1 } };
  expected_quals["InsertACGT,0,34"] = { { 39 , 1 } };
  expected_quals["InsertACGT,0,35"] = { { 39 , 1 } };
  expected_quals["InsertACGT,0,36"] = { { 39 , 1 } };
  expected_quals["InsertACGT,0,37"] = { { 39 , 1 } };
  expected_quals["InsertACGT,0,38"] = { { 39 , 1 } };
  expected_quals["InsertACGT,0,39"] = { { 39 , 1 } };
  expected_quals["InsertACGT,0,40"] = { { 39 , 1 } };
  expected_quals["InsertACGT,0,41"] = { { 39 , 1 } };
  expected_quals["InsertACGT,0,42"] = { { 39 , 1 } };
  expected_quals["InsertACGT,0,43"] = { { 39 , 1 } };
  expected_quals["InsertACGT,0,44"] = { { 39 , 1 } };
  expected_quals["InsertACGT,1,23"] = { {16 , 1 } };
  expected_quals["InsertACGT,1,24"] = { {16 , 1 } };
  expected_quals["InsertACGT,1,25"] = { {16 , 1 } };
  expected_quals["InsertACGT,1,26"] = { {16 , 1 } };
  expected_quals["InsertACGT,1,27"] = { {16 , 1 } };
  expected_quals["InsertACGT,1,28"] = { {16 , 1 } };
  expected_quals["InsertACGT,1,29"] = { {16 , 1 } };
  expected_quals["InsertACGT,1,30"] = { {16 , 1 } };
  expected_quals["InsertACGT,1,31"] = { {16 , 5 } };
  expected_quals["InsertACGT,1,32"] = { {16 , 1 } };
  expected_quals["InsertACGT,1,33"] = { {16 , 1 } };
  expected_quals["InsertACGT,1,34"] = { {16 , 1 } };
  expected_quals["InsertACGT,1,35"] = { {16 , 1 } };
  expected_quals["InsertACGT,1,36"] = { {16 , 1 } };
  expected_quals["InsertACGT,1,37"] = { {16 , 1 } };
  expected_quals["InsertACGT,1,38"] = { {16 , 1 } };
  expected_quals["InsertACGT,1,39"] = { {16 , 1 } };
  expected_quals["InsertACGT,1,40"] = { {16 , 1 } };
  expected_quals["InsertACGT,1,41"] = { {16 , 1 } };
  expected_quals["InsertACGT,1,42"] = { {16 , 1 } };
  expected_quals["InsertACGT,1,43"] = { {16 , 1 } };
  expected_quals["InsertACGT,1,44"] = { {16 , 1 } };
  expected_quals["DeleteGG,0,23"] = { { 15 , 1 } };
  expected_quals["DeleteGG,0,24"] = { { 15 , 1 } };
  expected_quals["DeleteGG,0,25"] = { { 15 , 1 } };
  expected_quals["DeleteGG,0,26"] = { { 15 , 1 } };
  expected_quals["DeleteGG,0,27"] = { { 15 , 1 } };
  expected_quals["DeleteGG,0,28"] = { { 15 , 1 } };
  expected_quals["DeleteGG,0,29"] = { { 15 , 1 } };
  expected_quals["DeleteGG,0,30"] = { { 0 , 2 }, { 15 , 1 } };
  expected_quals["DeleteGG,0,33"] = { { 15 , 1 } };
  expected_quals["DeleteGG,0,34"] = { { 15 , 1 } };
  expected_quals["DeleteGG,0,35"] = { { 15 , 1 } };
  expected_quals["DeleteGG,0,36"] = { { 15 , 1 } };
  expected_quals["DeleteGG,0,37"] = { { 15 , 1 } };
  expected_quals["DeleteGG,0,38"] = { { 15 , 1 } };
  expected_quals["DeleteGG,0,39"] = { { 15 , 1 } };
  expected_quals["DeleteGG,0,40"] = { { 15 , 1 } };
  expected_quals["DeleteGG,0,41"] = { { 15 , 1 } };
  expected_quals["DeleteGG,0,42"] = { { 15 , 1 } };
  expected_quals["DeleteGG,0,43"] = { { 15 , 1 } };
  expected_quals["DeleteGG,0,44"] = { { 15 , 1 } };
  expected_quals["DeleteGG,1,23"] = { { 40 , 1 } };
  expected_quals["DeleteGG,1,24"] = { { 40 , 1 } };
  expected_quals["DeleteGG,1,25"] = { { 40 , 1 } };
  expected_quals["DeleteGG,1,26"] = { { 40 , 1 } };
  expected_quals["DeleteGG,1,27"] = { { 40 , 1 } };
  expected_quals["DeleteGG,1,28"] = { { 40 , 1 } };
  expected_quals["DeleteGG,1,29"] = { { 40 , 1 } };
  expected_quals["DeleteGG,1,30"] = { { 0 , 2 }, { 40 , 1 } };
  expected_quals["DeleteGG,1,33"] = { { 40 , 1 } };
  expected_quals["DeleteGG,1,34"] = { { 40 , 1 } };
  expected_quals["DeleteGG,1,35"] = { { 40 , 1 } };
  expected_quals["DeleteGG,1,36"] = { { 40 , 1 } };
  expected_quals["DeleteGG,1,37"] = { { 40 , 1 } };
  expected_quals["DeleteGG,1,38"] = { { 40 , 1 } };
  expected_quals["DeleteGG,1,39"] = { { 40 , 1 } };
  expected_quals["DeleteGG,1,40"] = { { 40 , 1 } };
  expected_quals["DeleteGG,1,41"] = { { 40 , 1 } };
  expected_quals["DeleteGG,1,42"] = { { 40 , 1 } };
  expected_quals["DeleteGG,1,43"] = { { 40 , 1 } };
  expected_quals["DeleteGG,1,44"] = { { 40 , 1 } };
  expected_quals["StrandMismatchC>T,0,23"] = { { 39 , 1 } };
  expected_quals["StrandMismatchC>T,0,24"] = { { 39 , 1 } };
  expected_quals["StrandMismatchC>T,0,25"] = { { 39 , 1 } };
  expected_quals["StrandMismatchC>T,0,26"] = { { 39 , 1 } };
  expected_quals["StrandMismatchC>T,0,27"] = { { 39 , 1 } };
  expected_quals["StrandMismatchC>T,0,28"] = { { 39 , 1 } };
  expected_quals["StrandMismatchC>T,0,29"] = { { 39 , 1 } };
  expected_quals["StrandMismatchC>T,0,30"] = { { 39 , 1 } };
  expected_quals["StrandMismatchC>T,0,31"] = { { 39 , 1 } };
  expected_quals["StrandMismatchC>T,0,32"] = { { 39 , 1 } };
  expected_quals["StrandMismatchC>T,0,33"] = { { 39 , 1 } };
  expected_quals["StrandMismatchC>T,0,34"] = { { 39 , 1 } };
  expected_quals["StrandMismatchC>T,0,35"] = { { 39 , 1 } };
  expected_quals["StrandMismatchC>T,0,36"] = { { 39 , 1 } };
  expected_quals["StrandMismatchC>T,0,37"] = { { 39 , 1 } };
  expected_quals["StrandMismatchC>T,0,38"] = { { 39 , 1 } };
  expected_quals["StrandMismatchC>T,0,39"] = { { 39 , 1 } };
  expected_quals["StrandMismatchC>T,0,40"] = { { 39 , 1 } };
  expected_quals["StrandMismatchC>T,0,41"] = { { 39 , 1 } };
  expected_quals["StrandMismatchC>T,0,42"] = { { 39 , 1 } };
  expected_quals["StrandMismatchC>T,0,43"] = { { 39 , 1 } };
  expected_quals["StrandMismatchC>T,0,44"] = { { 39 , 1 } };
  expected_quals["StrandMismatchC>T,1,23"] = { {16 , 1 } };
  expected_quals["StrandMismatchC>T,1,24"] = { {16 , 1 } };
  expected_quals["StrandMismatchC>T,1,25"] = { {16 , 1 } };
  expected_quals["StrandMismatchC>T,1,26"] = { {16 , 1 } };
  expected_quals["StrandMismatchC>T,1,27"] = { {16 , 1 } };
  expected_quals["StrandMismatchC>T,1,28"] = { {16 , 1 } };
  expected_quals["StrandMismatchC>T,1,29"] = { {16 , 1 } };
  expected_quals["StrandMismatchC>T,1,30"] = { {16 , 1 } };
  expected_quals["StrandMismatchC>T,1,31"] = { {16 , 1 } };
  expected_quals["StrandMismatchC>T,1,32"] = { {16 , 1 } };
  expected_quals["StrandMismatchC>T,1,33"] = { {16 , 1 } };
  expected_quals["StrandMismatchC>T,1,34"] = { {16 , 1 } };
  expected_quals["StrandMismatchC>T,1,35"] = { {16 , 1 } };
  expected_quals["StrandMismatchC>T,1,36"] = { {16 , 1 } };
  expected_quals["StrandMismatchC>T,1,37"] = { {16 , 1 } };
  expected_quals["StrandMismatchC>T,1,38"] = { {16 , 1 } };
  expected_quals["StrandMismatchC>T,1,39"] = { {16 , 1 } };
  expected_quals["StrandMismatchC>T,1,40"] = { {16 , 1 } };
  expected_quals["StrandMismatchC>T,1,41"] = { {16 , 1 } };
  expected_quals["StrandMismatchC>T,1,42"] = { {16 , 1 } };
  expected_quals["StrandMismatchC>T,1,43"] = { {16 , 1 } };
  expected_quals["StrandMismatchC>T,1,44"] = { {16 , 1 } };

  int flank = 150;

  // initialise bam
  BamUtil bam_util;
  bam_util.open("testdata/test.bam");
  bam_util.loadHeader();
  bam_util.loadIndex();

  // initialise fasta reader
  FastaReader fasta_reader;
  fasta_reader.open("testdata/test.fa");

  // initialise bed_util, add beg_incl_flank and end_incl_flank coordinates
  BedUtil bed_util;
  bed_util.open("testdata/test.bed");
  bed_util.read();
  assert (bed_util.entries.size() == 3);
  bed_util.extendFlanks(bam_util, flank);
  bed_util.sequencesFromFasta(fasta_reader);

  // test pileup on first bed entry
  entry_t entry = bed_util.entries[0];
  bam_util.initialisePileup(&entry);

  // observed data
  std::map<std::string, std::string> observed_bases;
  std::map<std::string, std::map<int, int>> observed_quals;

  int min_map_qual = 0;

  int result;
  do {
    result = bam_util.nextColumn();
    const bam_pileup1_t *pl;
    int pos;
    int n_plp;
    int ref_len = bam_util.referenceLength(entry.rname);
    int tid = bam_util.templateIdentifier(entry.rname);
    while (pl = bam_plp_next(bam_util.buf, &tid, &pos, &n_plp)) {
      read_index r_idx = bam_util.indexReads(pl, n_plp, pos, &entry,
        min_map_qual);
      for (auto it = r_idx.begin(); it != r_idx.end(); it++) {       
        for (int i = 0; i < 2; i++) {
          std::stringstream ss;
          ss << it->first << "," << i << "," << pos + 1;
          observed_bases[ss.str()] = it->second[i].bases;
          std::map<int, int> quals = it->second[i].quals;
          for (auto it2 = quals.begin(); it2 != quals.end(); it2++) {
            observed_quals[ss.str()][it2->first]  = it2->second;
          }
        }
      }
    }
  } while (result > -1);

  // compare expected and observed bases
  for (auto it = expected_bases.begin(); it != expected_bases.end(); it++) {
    assert (it->second == observed_bases[it->first]);
  }

  // compare expected and observed quals
  for (auto it1 = expected_quals.begin(); it1 != expected_quals.end(); it1++) {
    for (auto it2 = it1->second.begin(); it2 != it1->second.end(); it2++) {
      assert (it2->second == observed_quals[it1->first][it2->first]);
    }
  }

}


void indexVariantsTest() {
  // expected variants
  std::map<int, std::map<int, std::map<std::string, std::map<int, int>>>>
    expected;
  // forward
  expected[0][23]["A,A"] = {{ 15, 4 }, { 39, 4 }};
  expected[0][24]["C,C"] = {{ 15, 4 }, { 39, 4 }};
  expected[0][25]["A,A"] = {{ 15, 4 }, { 39, 4 }};
  expected[0][26]["G,G"] = {{ 15, 4 }, { 39, 4 }};
  expected[0][27]["A,A"] = {{ 15, 4 }, { 39, 4 }};
  expected[0][28]["A,A"] = {{ 15, 4 }, { 39, 4 }};
  expected[0][29]["G,G"] = {{ 15, 4 }, { 39, 4 }};
  expected[0][30]["A,A"] = {{ 15, 2 }, { 39, 4 }};
  expected[0][30]["A-1G,A-1G"] = {{ 15, 1 }, { 0, 1 }};
  expected[0][30]["A-2GG,A-2GG"] = {{ 15, 1 }, { 0, 2 }};
  expected[0][31]["G,G"] = {{ 15, 2 }, { 39, 3 }};
  expected[0][31]["G+4ACGT,G+4ACGT"] = {{ 39, 5 }};
  expected[0][32]["G,G"] = {{ 15, 3 }, { 39, 4 }};
  expected[0][33]["C,C"] = {{ 15, 4 }, { 39, 3 }};
  expected[0][33]["C+1A,C+1A"] = {{ 39, 2 }};
  expected[0][34]["A,A"] = {{ 15, 4 }, { 39, 4 }};
  expected[0][35]["A,A"] = {{ 15, 4 }, { 39, 4 }};
  expected[0][36]["A,A"] = {{ 15, 4 }, { 39, 4 }};
  expected[0][37]["C,C"] = {{ 15, 3 }, { 39, 3 }};
  expected[0][37]["T,C"] = {{ 39, 1 }};
  expected[0][37]["T,T"] = {{ 15, 1 }};
  expected[0][38]["A,A"] = {{ 15, 4 }, { 39, 4 }};
  expected[0][39]["G,G"] = {{ 15, 4 }, { 39, 4 }};
  expected[0][40]["A,A"] = {{ 15, 4 }, { 39, 4 }};
  expected[0][41]["T,T"] = {{ 15, 4 }, { 39, 4 }};
  expected[0][42]["A,A"] = {{ 15, 4 }, { 39, 4 }};
  expected[0][43]["T,T"] = {{ 15, 4 }, { 39, 4 }};
  expected[0][44]["A,A"] = {{ 15, 4 }, { 39, 4 }};
  // reverse
  expected[1][23]["A,A"] = {{ 16, 4 }, { 40, 4 }};
  expected[1][24]["C,C"] = {{ 16, 4 }, { 40, 4 }};
  expected[1][25]["A,A"] = {{ 16, 4 }, { 40, 4 }};
  expected[1][26]["G,G"] = {{ 16, 4 }, { 40, 4 }};
  expected[1][27]["A,A"] = {{ 16, 4 }, { 40, 4 }};
  expected[1][28]["A,A"] = {{ 16, 4 }, { 40, 4 }};
  expected[1][29]["G,G"] = {{ 16, 4 }, { 40, 4 }};;
  expected[1][30]["A,A"] = {{ 16, 4 }, { 40, 2 }};
  expected[1][30]["A-1G,A-1G"] = {{ 40, 1 }, { 0, 1 }};
  expected[1][30]["A-2GG,A-2GG"] = {{ 40, 1 }, { 0, 2 }};
  expected[1][31]["G,G"] = {{ 16, 3 }, { 40, 2 }};
  expected[1][31]["G+4ACGT,G+4ACGT"] = {{ 16, 5 }};
  expected[1][32]["G,G"] = {{ 16, 4 }, { 40, 3 }};;
  expected[1][33]["C,C"] = {{ 16, 3 }, { 40, 4 }};
  expected[1][33]["C+1A,C+1A"] = {{ 16, 2 }};
  expected[1][34]["A,A"] = {{ 16, 4 }, { 40, 4 }};
  expected[1][35]["A,A"] = {{ 16, 4 }, { 40, 4 }};
  expected[1][36]["A,A"] = {{ 16, 4 }, { 40, 4 }};
  expected[1][37]["C,C"] = {{ 16, 3 }, { 40, 3 }};
  expected[1][37]["T,C"] = {{ 16, 1 }};
  expected[1][37]["T,T"] = {{ 40, 1 }};
  expected[1][38]["A,A"] = {{ 16, 4 }, { 40, 4 }};
  expected[1][39]["G,G"] = {{ 16, 4 }, { 40, 4 }};
  expected[1][40]["A,A"] = {{ 16, 4 }, { 40, 4 }};
  expected[1][41]["T,T"] = {{ 16, 4 }, { 40, 4 }};
  expected[1][42]["A,A"] = {{ 16, 4 }, { 40, 4 }};
  expected[1][43]["T,T"] = {{ 16, 4 }, { 40, 4 }};
  expected[1][44]["A,A"] = {{ 16, 4 }, { 40, 4 }};

  int flank = 150;

  // initialise bam
  BamUtil bam_util;
  bam_util.open("testdata/test.bam");
  bam_util.loadHeader();
  bam_util.loadIndex();

  // initialise fasta reader
  FastaReader fasta_reader;
  fasta_reader.open("testdata/test.fa");

  // initialise bed_util, add beg_incl_flank and end_incl_flank coordinates
  BedUtil bed_util;
  bed_util.open("testdata/test.bed");
  bed_util.read();
  assert (bed_util.entries.size() == 3);
  bed_util.extendFlanks(bam_util, flank);
  bed_util.sequencesFromFasta(fasta_reader);

  // test pileup on first bed entry
  entry_t entry = bed_util.entries[0];
  bam_util.initialisePileup(&entry);

  int min_map_qual = 0;

  // observed results
  std::map<int, std::map<int, std::map<std::string, std::map<int, int>>>>
    observed;

  int result;
  do {
    result = bam_util.nextColumn();
    const bam_pileup1_t *pl;
    int pos;
    int n_plp;
    int ref_len = bam_util.referenceLength(entry.rname);
    int tid = bam_util.templateIdentifier(entry.rname);
    while (pl = bam_plp_next(bam_util.buf, &tid, &pos, &n_plp)) {
      read_index r_idx = bam_util.indexReads(pl, n_plp, pos, &entry,
        min_map_qual);
      variant_index v_idx = bam_util.indexVariants(r_idx);
      for (auto it1 = v_idx.begin(); it1 != v_idx.end(); it1++) {
        for (auto it2 = it1->second.begin(); it2 != it1->second.end(); it2++) {
          for (auto it3 = it2->second.begin(); it3 != it2->second.end(); it3++) {
            observed[it2->first][pos+1][it1->first][it3->first] = it3->second;
          }      
        }
      }     
    }
  } while (result > -1);

  // compare expected and observed results
  for (auto it1 = expected.begin(); it1 != expected.end(); it1++) {
    for (auto it2 = it1->second.begin(); it2 != it1->second.end(); it2++) {
      for (auto it3 = it2->second.begin(); it3 != it2->second.end(); it3++) {
        for (auto it4 = it3->second.begin(); it4 != it3->second.end(); it4++) {
          int a = it4->second;
          int b = observed[it1->first][it2->first][it3->first][it4->first];
          assert (a == b);
        }
      }
    }
  }

}


void testWriteVariantHeader() {
  BamUtil bam_util;
  std::string observed = bam_util.writeVariantHeader();
  std::string expected = "amplicon,rname,pos,refbase,fwdbase,revbase,fq0,fq1,"
    "fq2,fq3,fq4,fq5,fq6,fq7,fq8,fq9,fq10,fq11,fq12,fq13,fq14,fq15,fq16,fq17,"
    "fq18,fq19,fq20,fq21,fq22,fq23,fq24,fq25,fq26,fq27,fq28,fq29,fq30,fq31,"
    "fq32,fq33,fq34,fq35,fq36,fq37,fq38,fq39,fq40,fq41,rq0,rq1,rq2,rq3,rq4,rq5,"
    "rq6,rq7,rq8,rq9,rq10,rq11,rq12,rq13,rq14,rq15,rq16,rq17,rq18,rq19,rq20,"
    "rq21,rq22,rq23,rq24,rq25,rq26,rq27,rq28,rq29,rq30,rq31,rq32,rq33,rq34,"
    "rq35,rq36,rq37,rq38,rq39,rq40,rq41";
  assert (observed.compare(expected) == 0);
}


int main(int argc, char **argv) {
  nonTargetedTest();
  targetedTest();
  indexReadsTest();
  indexVariantsTest();
  testWriteVariantHeader();
  std::cout << "[bam_util test] passed" << std::endl;
  return 0;
}
