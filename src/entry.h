/*
 * Copyright 2020 Inivata Inc.
 * Copyright 2020 Inivata Ltd.
 */

#ifndef ENTRY_H_
#define ENTRY_H_

typedef struct {
  std::string rname;
  int beg;
  int end;
  std::string id;
  std::string fwd;
  std::string rev;
  int beg_incl_flank;
  int end_incl_flank;
  std::string coord;
  std::string coord_incl_flank;
  char* sequence;
  char* sequence_incl_flank;
  int ref_len;
} entry_t;

#endif