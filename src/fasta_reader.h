/*
 * Copyright 2020 Inivata Inc.
 * Copyright 2020 Inivata Ltd.
 */

#ifndef FASTA_READER_H_
#define FASTA_READER_H_

#include <string.h>
#include <string>
#include <sstream>
#include <stdexcept>
#include "../third_party/htslib-1.3.1/htslib/faidx.h"


class FastaReader {

  public:

    faidx_t* fai;

    void open(const char* fasta);

    char* sequence(std::string coord);

    int referenceLength(std::string rname);

    void close();

};

#endif
