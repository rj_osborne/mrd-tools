/*
 * Copyright 2020 Inivata Inc.
 * Copyright 2020 Inivata Ltd.
 *
 * For each primer build a set of edits within a Levenshtein distance of 2
 * Create a trie for all edited forward and reverse primers
 *
 */


#include "primer_trie.h"

char ALPH[] = { 'A', 'C', 'G', 'T' };
int ALPH_LEN = 4;


/**
 *  Build Trie
 *  @params primers : map of { primer name : primer sequence }
 */
void PrimerTrie::build(std::map<std::string, std::string> primers) {
  this->depth = 0;
  this->root = new Trie();
  for (auto it1 = primers.begin(); it1 != primers.end(); it1++) {
    std::string name = it1->first;
    std::set<std::string> edits = PrimerTrie::edits2(it1->second);
    for (auto it2 = edits.begin(); it2 != edits.end(); it2++) {
      Trie* curr = this->root;
      std::string sequence = *it2;
      PrimerTrie::insert(name, sequence);
    }
  }
}


/**
 * Insert a sequence into the trie
 * @param name : primer name
 * @param sequence : primer sequence
 */
void PrimerTrie::insert(std::string name, std::string sequence) {
  Trie* curr = this->root;
  for (int i = 0; i < sequence.length(); i++) {
    char character = sequence[i];
    if (curr->succs[character] == NULL) {
      curr->succs[character] = new Trie();
    }
    curr = curr->succs[character];
  }
  curr->name.insert(name);
  if (sequence.length() > depth) {
    this->depth = sequence.length();
  }
} 
  

/**
 *  Search Trie
 *  @params primers : map of { primer name : primer sequence }
 */
std::string PrimerTrie::search(std::string sequence) {
  std::set<std::string> result;
  Trie* curr = this->root;
  for (int i = 0; i < sequence.length(); i++) {
    // intermediate results
    if (curr->succs[sequence[i]] != NULL) {
      curr = curr->succs[sequence[i]];
      if (curr->name.size() > 0) {
        result = curr->name;
      }
    }
    // no successor - break out of trie search
    else {
      break;
    }
  }
  // return the primer name if leaf contains only one match
  if (result.size() == 1) {
    for (auto it = result.begin(); it != result.end(); it++) {
      return *it;
    }
  }
  return "unknown";
}


/**
 * Create all strings within one edit of the initial string: includes
 * substitutions, deletions and insertions with canonical A, C, G, T bases
 * @param str : initial string
 * @returns set of strings within one edit of str
 */
std::set<std::string> PrimerTrie::edits1(std::string str) {
  std::set<std::string> results;
  for (int i = 0; i < str.size(); i++) { 
    results.insert(str.substr(0, i) + str.substr(i + 1)); 
  }
  for (int j = 0; j < ALPH_LEN; j++) {
    for (int i = 0; i < str.size(); i++)  { 
      results.insert(str.substr(0, i) + ALPH[j] + str.substr(i + 1));
    }
    for (int i = 0; i < str.size() + 1; i++) {
      results.insert(str.substr(0, i) + ALPH[j] + str.substr(i) );
    }
  }
  return results;
}


/**
 * Create all strings within two edits of the initial string: includes
 * substitutions, deletions and insertions with canonical A, C, G, T bases
 * @param str : initial string
 * @returns set of strings within two edits of str
 */
std::set<std::string> PrimerTrie::edits2(std::string str) {
  std::set<std::string> results;
  std::set<std::string> e1 = PrimerTrie::edits1(str);
  for (auto it = e1.begin(); it != e1.end(); it++) {
    std::set<std::string> e2 = PrimerTrie::edits1(*it);
    for (auto it2 = e2.begin(); it2 != e2.end(); it2++) {
      results.insert(*it2);
    }
  }
  return results;
}
