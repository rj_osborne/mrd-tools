/*
 * Copyright 2020 Inivata Inc.
 * Copyright 2020 Inivata Ltd.
 */

#include "bed_util.h"


/*
 * Open the bed file
 * @param bed : bed filename
 */
void BedUtil::open(const char* bed) {
  this->bed_stream = new std::ifstream(bed, std::ifstream::in);
  if (this->bed_stream->fail()) {
    std::stringstream er;
    er << "[bed_reader] error: failed to open input BED file ";
    er << bed;
    er << std::endl;
    throw std::runtime_error(er.str());
  }
}


/*
 * Save the header and bed file entries
 */
void BedUtil::read() {
  std::string bed_line;
  std::stringstream ss;
  while (std::getline(*this->bed_stream, bed_line)) {
    if (bed_line[0] == '#') {
      ss << bed_line;
    }
    else {
      this->entries.push_back(BedUtil::nextEntry(bed_line));
    }
  }
  this->header = ss.str();    
}


/*
 * Get the next entry from the bed file
 * @param bed_line : line from bed file
 * @return entry structure
 */
entry_t BedUtil::nextEntry(std::string bed_line) {
  std::stringstream ss;
  ss.str(bed_line);
  std::string item;
  std::vector<std::string> row;
  while (std::getline(ss, item, '\t')) {
    row.push_back(item);
  }
  entry_t entry;
  if (row.size() == 4) {
    entry.rname = row[0];
    entry.beg   = std::stoi(row[1]);
    entry.end   = std::stoi(row[2]);
    entry.id    = row[3];
  }
  else if (row.size() == 6) {
    entry.rname = row[0];
    entry.beg   = std::stoi(row[1]);
    entry.end   = std::stoi(row[2]);
    entry.id    = row[3];
    entry.fwd   = row[4];
    entry.rev   = row[5];
  }
  else {
    std::stringstream er;
    er << "[bed_reader] error: row must contain 4 or 6 fields ";
    er << std::endl;
    throw std::runtime_error(er.str());  
  }
  return entry;
}


/*
 * Index the bed file using the reference name
 */
void BedUtil::index() {
  for (int i = 0; i < this->entries.size(); i++) {
    entry_t entry = this->entries[i];
    this->idx[entry.id] = entry;
  }
}


/*
 * Close bed file
 */
void BedUtil::close() {
  this->bed_stream->close();
}


/**
 * Extending flanking coordinates
 * @param bam_util : bam reader structure
 * @param flank : number of flanking bases
 */
void BedUtil::extendFlanks(BamUtil bam_util, int flank) {
  for (auto it = this->entries.begin(); it != this->entries.end(); ++it) {
    it->beg_incl_flank = std::max(1, it->beg - flank);
    it->ref_len = bam_util.referenceLength(it->rname);
    it->end_incl_flank = std::min(it->end + flank, it->ref_len);
  }
}


/**
 * Sequence including flanks
 * @param fasta : fasta reader structure
 */
void BedUtil::sequencesFromFasta(FastaReader fasta_reader) {
  for (auto it = this->entries.begin(); it != this->entries.end(); ++it) {
    it->coord_incl_flank = BedUtil::coordinate(it->rname, it->beg_incl_flank,
      it->end_incl_flank);
    it->sequence_incl_flank = fasta_reader.sequence(it->coord_incl_flank);
  }
}


/**
 * Coordinate string in rname:beg-end format
 * @param entry : initial entry_t structure
 * @return : updated entry_t structure
 */
std::string BedUtil::coordinate(std::string rname, int beg, int end) {
  std::stringstream ss;
  ss << rname;
  ss << ":";
  ss << beg;
  ss << "-";
  ss << end;
  return ss.str();
}
