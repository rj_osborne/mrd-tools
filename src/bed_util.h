/*
 * Copyright 2020 Inivata Inc.
 * Copyright 2020 Inivata Ltd.
 */

#ifndef BED_UTIL_H_
#define BED_UTIL_H_


#include <string.h>
#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

#include "bam_util.h"
#include "entry.h"
#include "fasta_reader.h"


class BedUtil {

  public:

    std::ifstream* bed_stream;

    std::string header;

    std::vector<entry_t> entries;

    std::map<std::string, entry_t> idx;

    void open(const char* bed);

    entry_t nextEntry(std::string bed_line);

    void read();

    void index();

    void close();

    void extendFlanks(BamUtil bam_util, int flank);

    void sequencesFromFasta(FastaReader fasta_reader);

    std::string coordinate(std::string rname, int beg, int end);

};

#endif
