/*
 * Copyright 2020 Inivata Inc.
 * Copyright 2020 Inivata Ltd.
 */

#include "bam_util.h"


std::map<char, char> COMPLEMENT = {{ 'A', 'T' }, { 'C', 'G' }, { 'G', 'C' },
  { 'T', 'A' }, { 'N', 'N' } };

int MAXSCORE = 42;  // maximum allowable Phred score


/**
 * Open bam file
 * @param bam : bam filename
 */
void BamUtil::open(const char* bam) {
  this->bam = bam;
  this->in = hts_open(bam, "r");
}


/**
 * Close bam file
 */
void BamUtil::close() {
  hts_close(this->in);
}


/**
 * Load bam header
 */
void BamUtil::loadHeader() {
  this->head = sam_hdr_read(this->in);
}


/**
 * Close bam header
 */
void BamUtil::closeHeader() {
  bam_hdr_destroy(this->head);
}


/**
 * Load bam index
 */
void BamUtil::loadIndex() {
  this->idx = sam_index_load(this->in, this->bam);
}


/**
 * Close bam index
 */
void BamUtil::closeIndex() {
  hts_idx_destroy(this->idx);
}


/**
 * Initialise a targeted read iterator
 * @param coord : coordinate string
 */
void BamUtil::initialiseTargetIterator(std::string coord) {
  this->iter = sam_itr_querys(this->idx, this->head, coord.c_str());
}


/**
 * Destroy a targeted read iterator
 */
void BamUtil::destroyTargetIterator() {
  sam_itr_destroy(this->iter);
}


/** 
 * Initialise an alignment structure
 */
void BamUtil::initialiseAlignment() {
  this->b = bam_init1();
}


/** 
 * Destroy an alignment structure
 */
void BamUtil::destroyAlignment() {
  bam_destroy1(this->b);
}


/**
 * Return the next read from a targeted iterator
 * @returns integer < 0 if no more reads
 */
int BamUtil::nextTargetRead() {
  return sam_itr_next(this->in, this->iter, this->b);
}


/**
 * Return the next read from a non-targeted iterator
 * @returns integer < 0 if no more reads
 */
int BamUtil::nextRead() {
  return sam_read1(this->in, this->head, this->b);
}


/**
 * Return the next non-supplementary read from a non-targeted iterator
 * @returns integer < 0 if no more reads
 */
int BamUtil::nextRepresentativeRead() {
  int ret = BamUtil::nextRead();
  while (BamUtil::readIsSupplementary() == true) {
    ret = BamUtil::nextRead();
  }
  return ret;
}


/**
 * Return is supplementary
 * @return boolean : true if read is supplementary
 */
bool BamUtil::readIsSupplementary() {
  return  (this->b->core.flag & BAM_FSUPPLEMENTARY)? true : false;
}


/**
 * Read is usable
 * @return boolean : true if read is usable
 */
bool BamUtil::readIsUsable() {
  int suppl     = BamUtil::readIsSupplementary();
  int unmapped  = (this->b->core.flag & BAM_FUNMAP)? 1 : 0;
  int secondary = (this->b->core.flag & BAM_FSECONDARY)? 1 : 0;
  int qcfail    = (this->b->core.flag & BAM_FQCFAIL)? 1 : 0;
  if ((suppl + unmapped + secondary + qcfail) == 0) {
    return true;
  }
  return false;
}


/**
 * Return the template identifier
 * @param rname : reference name string
 * @returns integer : tid
 */
int BamUtil::templateIdentifier(std::string rname) {
  return bam_name2id(this->head, rname.c_str());
}


/**
 * Return the reference name
 * @return reference name or "unmapped" if read is not mapped
 */
std::string BamUtil::referenceName() {
  if (this->b->core.tid < 0) {
    return "unmapped";
  }
  return this->head->target_name[this->b->core.tid];
}


/**
 * Return the query name
 */
std::string BamUtil::queryName() {
  return bam_get_qname(this->b);
}


/**
 * Return the left coordinate
 */
int BamUtil::leftCoordinate() {
  return this->b->core.pos + 1;
}


/**
 * Return the right coordinate
 */
int BamUtil::rightCoordinate() {
  return bam_endpos(this->b);
}


/**
 * Read is on forward strand
 * @return true if the read is forward
 */
bool BamUtil::readIsForward() {
  return !bam_is_rev(this->b);
}


/**
 * Read is on reverse strand
 * @return true if the read is reverse
 */
bool BamUtil::readIsReverse() {
  return bam_is_rev(this->b);
}


/**
 * Return read strand
 * @return integer
 *   0 if read is forward
 *   1 if read is reverse
 *  -1 if read is not
 */
int BamUtil::readStrand() {
  if (this->b->core.flag & BAM_FREVERSE) {
    return 1;
  }
  else if (this->b->core.flag & BAM_FMREVERSE) {
    return 0;
  }
  else {
    return -1;
  }
}


/**
 * Return read number
 * @return integer (1 or 2)
 */
int BamUtil::readNumber() {
  if (this->b->core.flag & BAM_FREAD1) {
    assert ((this->b->core.flag & BAM_FREAD2) == 0);
    return 1;
  }
  else {
    assert (this->b->core.flag & BAM_FREAD2);
    return 2;
  }
}


/**
 * Mapping quality
 * @return integer mapping quality
 */
int BamUtil::mapQuality() {
  return this->b->core.qual;
}


/*
 * Returns prefix in forward read orientation
 * @param prefix_len : length of prefix
 * @return uppercase prefix string
 */
std::string BamUtil::fwdPrefix(int prefix_len) {
  std::stringstream ss;
  for (int i = 0; i < prefix_len; i++) {
    ss << seq_nt16_str[bam_seqi(bam_get_seq(this->b),i)];
  }
  return ss.str();
}


/*
 * Returns prefix in original reverse read orientation
 * (i.e. reverse complement)
 * @param prefix_len : length of prefix
 * @return uppercase prefix string
 */
std::string BamUtil::revPrefix(int prefix_len) {
  std::stringstream ss;
  int beg = this->b->core.l_qseq - prefix_len - 1;
  int end = this->b->core.l_qseq - 1;
  for (int i = end; i > beg; i--) {
    ss << COMPLEMENT[toupper(seq_nt16_str[bam_seqi(bam_get_seq(this->b),i)])];
  }
  return ss.str();
}


/*
 * Reference sequence length
 * @param rname: reference name
 * @return reference sequence length
 */
int BamUtil::referenceLength(std::string rname) {
  int tid = BamUtil::templateIdentifier(rname);
  return this->head->target_len[tid];
}


/**
 * Initialise a pileup
 * @param entry : bed entry structure
 */
void BamUtil::initialisePileup(entry_t *entry) {
  BamUtil::initialiseAlignment();
  BamUtil::initialiseTargetIterator(entry->coord_incl_flank);
  this->buf  = bam_plp_init((bam_plp_auto_f)bam_read1, this->in);
  bam_plp_set_maxcnt(this->buf, INT_MAX);
}


/**
 * Move to the next pileup column
 * @return int : 0 if no more columns
 */
int BamUtil::nextColumn() {
  int result = sam_itr_next(this->in, this->iter, this->b);
  if (result > -1) {
    if (BamUtil::readIsUsable() == true) {
      bam_plp_push(this->buf, this->b);
    }
  }
  else if (result == -1) {
    bam_plp_push(this->buf, 0);
  }
  else if (result < -1) {
      std::stringstream er;
      er << "Error : error code ";
      er << result;
      er << " encountered reading sam iterator.";
      er << std::endl;
      throw std::runtime_error(er.str());
  }
  return result;
}


/**
 * Base call for one alignment covering the pileup position
 * @param p   : structure for one alignment covering the pileup position
 * @param pos : pileup position
 * @param entry : structure for one BED entry
 * @return string : base
 */
std::string BamUtil::bases(const bam_pileup1_t *p, int pos, entry_t *entry) {
  std::stringstream ss;
  if (!p->is_del) {
    if (p->qpos < p->b->core.l_qseq) {
      ss << char(seq_nt16_str[bam_seqi(bam_get_seq(p->b), p->qpos)]);
    }
    else {
      ss << "N";
    }
  }
  else {
    ss << "*";
  }
  // insertions
  if (p->indel > 0) {
    ss << "+";
    ss << p->indel;
    for (int j = 1; j <= p->indel; j++) {
      ss << char(seq_nt16_str[bam_seqi(bam_get_seq(p->b), p->qpos + j)]);
    }
  }
  // deletions
  else if (p->indel < 0) {
    ss << "-";
    ss << -p->indel;
    for (int j = 1; j <= -p->indel; j++) {
      // position of base in sequence_incl_flanks string
      int coord = pos - entry->beg_incl_flank + j + 1;
      if (coord > entry->ref_len) {
        ss << "N";
      } 
      else {
        ss << entry->sequence_incl_flank[coord];
      }
    }
  }
  return ss.str();
}


/**
 * Base qualities for one alignment covering the pileup position
 * @param p : structure for one alignment covering the pileup position
 * @return map : { base quality : count }
 */
std::map<int, int> BamUtil::quals(const bam_pileup1_t *p) {
  std::map<int, int> quals;
  if (!p->is_del) {
    quals[bam_get_qual(p->b)[p->qpos]]++;
  } 
  // insertion
  if (p->indel > 0) {
    for (int j = 1; j <= p->indel; j++) {
      quals[bam_get_qual(p->b)[p->qpos + j]]++;
    }
  // deletion
  } else if (p->indel < 0) {
      for (int j = 1; j <= -p->indel; j++) {
        quals[0]++;
      }
  }
  return quals;
}


/**
 * Count variants
 * @param entry : bed entry structure
 */
void BamUtil::pileupEntry(entry_t *entry, int min_map_qual) {
  const bam_pileup1_t *pl;
  int pos;
  int n_plp;
  int ref_len = BamUtil::referenceLength(entry->rname);
  int tid = BamUtil::templateIdentifier(entry->rname);
  while (pl = bam_plp_next(this->buf, &tid, &pos, &n_plp)) {
    read_index r_idx = BamUtil::indexReads(pl, n_plp, pos, entry,
      min_map_qual);
    variant_index v_idx = BamUtil::indexVariants(r_idx);
    BamUtil::writeVariants(entry, pos, v_idx);
  }
}


/**
 * Index reads by orientation and query name
 * @param entry : bed entry structure
 */
read_index BamUtil::indexReads(const bam_pileup1_t *pl, int n_plp, int pos,
    entry_t *entry, int min_map_qual) {
  read_index r_idx;
  for (int i = 0; i < n_plp; i++) {
    const bam_pileup1_t *p = pl + i;
     // I am not sure why but this->b = p->b; throws a 
     // sam.c:1572: resolve_cigar2: Assertion `s->k < c->n_cigar' failed.
    bam1_t *b = p->b;
    if  (b->core.qual >= min_map_qual) {
      std::string qname = bam_get_qname(b);
      int is_reverse = bam_is_rev(b);
      r_idx[qname][is_reverse].bases = BamUtil::bases(p, pos, entry);
      r_idx[qname][is_reverse].quals = BamUtil::quals(p);
    }
  }
  return r_idx;
}


/**
 * Index variants counts by base quality scores
 * @param r_idx : reads indexed by query name and orientation
 * @returns : variants indexed by base quality score
 */
variant_index BamUtil::indexVariants(read_index r_idx) {
  variant_index v_idx;
  for (auto it1 = r_idx.begin(); it1 != r_idx.end(); it1++) {
    if (it1->second.size() > 1) {
      // string of fwd base, rev base
      std::stringstream ss;
      ss << it1->second[0].bases << "," << it1->second[1].bases;
      for (int i = 0; i < 2; i++) {
        std::map<int, int> quals = it1->second[i].quals;
        for (auto it2 = quals.begin(); it2 != quals.end(); it2++) {
          v_idx[ss.str()][i][it2->first] += it2->second;
        }
      }
    }
  }
  return v_idx;
}


/**
 * Write variants
 * @param entry : bed entry structure
 * @param pos : genomic position
 * @param v_idx : variant counts indexed by base quality scores
 */
void BamUtil::writeVariants(entry_t *entry, int pos, variant_index v_idx) {
  for (auto it = v_idx.begin(); it != v_idx.end(); it++) {
    std::stringstream ss;
    ss << entry->id << "," << entry->rname << "," << pos + 1 << ","
       << entry->sequence_incl_flank[pos - entry->beg_incl_flank + 1] << ","
       << it->first << ",";
    int j;
    // fwd counts
    for (j = 0; j < MAXSCORE; j++) {
      ss << it->second[0][j] << ",";
    }
    // rev counts
    for (j = 0; j < MAXSCORE - 1; j++) {
      ss << it->second[1][j] << ",";
    }
    ss << it->second[1][j+1];
    std::cout << ss.str() << std::endl;
  }
}


/**
 * Write out a csv file header
 */
std::string BamUtil::writeVariantHeader() {
  std::stringstream ss;
  ss << "amplicon,rname,pos,refbase,fwdbase,revbase,";
  int i;
  for (i = 0; i < MAXSCORE; i++) {
    ss << "fq" << i << ",";
  }
  for (i = 0; i < MAXSCORE - 1; i++) {
    ss << "rq" << i << ",";
  }
  ss << "rq" << i;
  return ss.str();
}


/**
 * Build a read table (used for primer qc)
 * @return read structure
 */
read_t BamUtil::readTable(int min_base_qual, int min_map_qual) {
  read_t rt;
  rt.qname     = this->queryName();
  rt.read_no   = this->readNumber();
  rt.mq        = this->mapQuality() >= min_map_qual;
  rt.bq        = this->baseQualities(min_base_qual);
  rt.strand    = this->readStrand();
  rt.is_usable = this->readIsUsable();
  rt.rname     = this->referenceName();
  rt.beg       = this->leftCoordinate();
  rt.end       = this->rightCoordinate();
  return rt;
}


/**
 * Count base quality scores
 * @returns : map { score : count of bases }
 */
std::map<int,int> BamUtil::baseQualities(int min_base_qual) {
  uint8_t *qual = bam_get_qual(this->b);
  std::map<int, int> counts;
  for (int i = 0; i < this->b->core.l_qseq; i++) { 
    counts[(qual[i] >= min_base_qual)]++;
  }
  return counts;
}
