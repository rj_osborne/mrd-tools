  /*
 * Copyright 2020 Inivata Inc.
 * Copyright 2020 Inivata Ltd.
 */

#ifndef PRIMER_TRIE_H_
#define PRIMER_TRIE_H_

#include <set> 
#include <iostream>
#include <string>
#include <algorithm>
#include <string.h>
#include <map>
#include <cassert>
#include <vector>


struct Trie {
  std::map<char, Trie*> succs;
  std::set<std::string> name;
};


class PrimerTrie {

  public:
  
    Trie* root;
    
    int depth;
    
    void insert(std::string name, std::string sequence);

    void build(std::map<std::string, std::string> primers);

    std::string search(std::string sequence);

    std::set<std::string> edits1(std::string str);

    std::set<std::string> edits2(std::string str);

};

#endif
