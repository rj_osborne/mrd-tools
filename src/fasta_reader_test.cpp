/*
 * Copyright 2020 Inivata Inc.
 * Copyright 2020 Inivata Ltd.
 */

#include <assert.h>
#include <iostream>
#include <vector>
#include "fasta_reader.h"


void fastaReaderTest() {
  FastaReader fasta_reader;
  fasta_reader.open("testdata/test.fa");
  assert(fasta_reader.fai != NULL);
  std::vector<std::string> coords = { "chr1:1-69", "chr2:1-230",
    "chrX:1-280" };
  std::vector<std::string> sequences = {
     "AAAAACACAGAGACACAGAGACACAGAAGAGGCAAACAGATATAGGTAGAGAGACACACAGTTTGAGAG",
     "GTTATGACTTTTGTACAGACCTTTACACCTAGGTATTCTGAAGTTGGTGGTCTGTGAGTGGCATCGAGT"
     "GATGACTGACACACTCTGACCTGGGGTAGAACAACACGTCTCCCTGCAGTTTGCTGAATTTTAGGGCGT"
     "CACCACCTGTTTTCAAGAGTGTGTTTTTCTATCCTCCCGAGTTTTGCCCACCTAAGCAATGGTTTTGCT"
     "GTAATAAAGAATTACACTATTTA",
     "CCCTAACCCTAACCCTAACCCTAACCCTCTGAAAGTGGACCTATCAGCAGGATGTGGGTGGGAGCAGAT"
     "TAGAGAATAAAAGCAGACTGCCTGAGCCAGCAGTGGCAACCCAATGGGGTCCCTTTCCATACTGTGGAA"
     "GCTTCGTTCTTTCACTCTTTGCAATAAATCTTGCTATTGCTCACTCTTTGGGTCCACACTGCCTTTATG"
     "AGCTGTGACACTCACCGCAAAGGTCTGCAGCTTCACTCCTGAGCCAGTGAGACCACAACCCCACCAGAA"
     "AGAA" };
  for (int i = 0; i < coords.size(); i++) {
    std::string s1 = sequences[i];
    std::string s2 = fasta_reader.sequence(coords[i]);
    assert(s1.compare(s2) == 0);
  }
  std::string rname = "chr1";
  assert(fasta_reader.referenceLength(rname) == 69);
  fasta_reader.close();
}

int main(int argc, char **argv) {
  fastaReaderTest();
  std::cout << "[fasta_reader test] passed" << std::endl;
}
