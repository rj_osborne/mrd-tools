/**
   Copyright (C) 2020 Robert Osborne
   Copyright (C) 2020 Inivata

  g++ mpileup.cpp -o mpileup htslib-1.10/libhts.a -llzma -lbz2 -lpthread  -lcurl -g -std=c++0x

**/

#include <map>
#include <iostream>
#include <sstream>
#include <stdexcept>

#include "htslib-1.10/htslib/sam.h"
#include "htslib-1.10/htslib/faidx.h"


// fixed parameters
int MASK = BAM_FUNMAP | BAM_FSECONDARY | BAM_FQCFAIL | BAM_FSUPPLEMENTARY;


typedef struct {
  std::string fwd;
  std::string rev;
  int n;
} overlap_t;


/**
 * Sequence from pileup column and read intersection
 * @param p : pileup structure
 * @param pos : current position
 * @param beg : beg coordinate
 * @param end : end coordinate
 * @param ref_len : length of reference sequence
 * @param ref : reference sequence
 * @returns : sequence from pileup column and read intersection
 */
std::string sequence(const bam_pileup1_t *p, int pos, int beg, int end,
    int ref_len, char* ref) {
  std::stringstream ss;
  if (!p->is_del) {
    if (p->qpos < p->b->core.l_qseq) {
      ss << char(seq_nt16_str[bam_seqi(bam_get_seq(p->b), p->qpos)]);
    }
    else {
      ss << "N";
    }
  }
  else {
    ss << "*";
  }
  // insertions
  if (p->indel > 0) {
    ss << "+";
    ss << p->indel;
    for (int j = 1; j <= p->indel; j++) {
      ss << char(seq_nt16_str[bam_seqi(bam_get_seq(p->b), p->qpos + j)]);
    }
  }
  // deletions
  else if (p->indel < 0) {
    ss << "-";
    ss << -p->indel;
    for (int j = 1; j <= -p->indel; j++) {
      // position of base in ref string
      int coord = pos - beg + j + 1;
      if (coord > ref_len) {
        ss << "N";
      }
      else {
        ss << ref[coord];
      }
    }
  }
  return ss.str();
}


/**
 * Count paired-end overlapping reads from one column
 * @param pl : pileup structure
 * @param rname : reference name
 * @param pos : current position
 * @param n_plp : number of reads in the pileup column
 * @param beg : beg coordinate
 * @param end : end coordinate
 * @param min_base_qual : minimum base quality
 * @param min_map_qual : minimum mapping quality
 * @param ref_len : length of reference sequence
 * @param ref : reference sequence
 */
void column(const bam_pileup1_t *pl, const char* rname, int pos, int n_plp,
    int beg, int end, int min_base_qual, int min_map_qual, int ref_len,
    char* ref) {
  std::map<std::string, overlap_t> overlaps;
  std::map<std::string, int> counts;
  // iterate through each column that overlaps with the target
  if ((pos + 1) >= beg && (pos + 1) <= end) {
    for (int i = 0; i < n_plp; i++) {
      const bam_pileup1_t *p = pl + i;
      int c = p->qpos < p->b->core.l_qseq ? bam_get_qual(p->b)[p->qpos] : 0;
      if ((c >= min_base_qual) && (p->b->core.qual >= min_map_qual)) {
        std::string qname = bam_get_qname(p->b);
        overlap_t* overlap = &overlaps[qname];
        if (bam_is_rev(p->b)) {
          overlap->rev = sequence(p, pos, beg, end, ref_len, ref);
          overlap->n++;
        }
        else {
          overlap->fwd = sequence(p, pos, beg, end, ref_len, ref);
          overlap->n++;
        }
        // count fwd,rev calls where paired-end reads overlap
        if (overlap->n == 2) {
          std::stringstream ss;
          ss << overlap->fwd << "," << overlap->rev;
          counts[ss.str()]++;
          overlaps.erase(qname);
        }
      }
    }
  }
  // summarise counts
  for (auto it = counts.begin(); it != counts.end(); it++) {
    std::cout << rname << "," << pos + 1 << "," << ref[pos - beg + 1] << ","
      << it->first << "," << it->second << std::endl;
  }
}


/**
 * Pileup target region
 * @param min_map_qual : minimum mapping quality
 * @param min_base_qual : minimum base quality
 * @param depth : maximum pileup depth
 * @param pl : pileup structure
 * @param bam : bam filename
 * @param fasta : fasta filename 
 * @param rname : reference name
 * @param beg : beg coordinate
 * @param end : end coordinate
 */
int pileup(int min_map_qual, int min_base_qual, int depth, const char* bam,
    const char* fasta, const char* rname, int beg, int end) {
  // region
  std::stringstream ss;
  ss << rname << ":" << beg << "-" << end;
  std::string region = ss.str();  
  // open bam file
  htsFile *in = hts_open(bam, "r");
  if (in == NULL) {
    return 1;
  }
  // bam file index
  hts_idx_t *idx = sam_index_load(in, bam); 
  if (idx == 0) {
    return 1;
  }
  // bam file header
  bam_hdr_t *head = sam_hdr_read(in);
  int tid = bam_name2id(head, rname);
  if (tid < 0) {
    return 1;
  }
  // fasta file
  faidx_t* fai = fai_load(fasta);
  int ref_len;
  char* ref = fai_fetch(fai, region.c_str(), &ref_len);
  if (ref == NULL) {
    return 1;
  }
  // initilise pileup
  hts_itr_t *iter = sam_itr_querys(idx, head, region.c_str());
  bam_plp_t buf = bam_plp_init((bam_plp_auto_f)bam_read1, in);
  bam_plp_set_maxcnt(buf, depth);
  bam1_t *b = bam_init1();
  int pos;
  int n_plp = -1;
  const bam_pileup1_t *pl;
  // pileup
  int result;
  while ((result = sam_itr_next(in, iter, b)) >= 0) {
    if ((b->core.flag & MASK) == 0) {
      bam_plp_push(buf, b); 
    }
    while ((pl=bam_plp_next(buf, &tid, &pos, &n_plp)) != 0) {
      column(pl, rname, pos, n_plp, beg, end, min_base_qual, min_map_qual,
        ref_len, ref);
    }
  }
  if (result < -1) {
    std::cout << "Error code : " << result 
      << " encountered reading sam iterator" << std::endl;
    return 1;
  }
  // finalize pileup
  bam_plp_push(buf,0);
  while ((pl=bam_plp_next(buf, &tid, &pos, &n_plp)) != 0) {
    column(pl, rname, pos, n_plp, beg, end, min_base_qual, min_map_qual,
      ref_len, ref);
  }
  sam_itr_destroy(iter);
  hts_idx_destroy(idx);
  bam_destroy1(b);
  bam_hdr_destroy(head);
  bam_plp_destroy(buf);
  hts_close(in);
}


/**
 * Print out usage
 */
void usage() {
  fprintf(stderr, "\n[MRD pileup] Usage:\n");
  fprintf(stderr, "\t-q\tminimum mapping quality (default 30)\n");
  fprintf(stderr, "\t-Q\tminimum base quality (default 30)\n");
  fprintf(stderr, "\t-d\tmaximum depth (default 1000000)\n");
  fprintf(stderr, "\t-B\tBAM file\n");
  fprintf(stderr, "\t-F\tFASTA file\n");
  fprintf(stderr, "\t-r\treference name\n");
  fprintf(stderr, "\t-b\tbeginning coordinate\n");
  fprintf(stderr, "\t-e\tend coordinate\n");
  fprintf(stderr, "\t-h\tHelp\n");
  exit(1);
}


int main(int argc, char **argv) {
  if (argc == 1) {
    usage();
  }
  int min_map_qual = 30;
  int min_base_qual = 30;
  int depth = 1000000;
  const char* bam = NULL;
  const char* fasta = NULL;
  const char* rname;
  int beg;
  int end;
  int opt;
  while ((opt = getopt(argc, argv, "q:Q:d:B:F:r:b:e:h")) >= 0) {
    switch (opt) {
      case 'q':
        min_map_qual = std::stoi(optarg);
        break;
      case 'Q':
        min_base_qual = std::stoi(optarg);
        break;
      case 'd':
        depth = std::stoi(optarg);
        break;
      case 'B':
        bam = optarg;
        break;
      case 'F':
        fasta = optarg;
        break;
      case 'r':
        rname = optarg;
        break;
      case 'b':
        beg = std::stoi(optarg);
        break;
      case 'e':
        end = std::stoi(optarg);
        break;
      case 'h':
        usage();
        break;
      default:
        usage();
        break;
    }
  }
  if ((bam == NULL) || (fasta == NULL)) {
    usage();
  }
  return pileup(min_map_qual, min_base_qual, depth, bam, fasta, rname, beg,
    end);
}
