all: htslib compile test clean

htslib:
	@echo "Installing htslib version 1.3.1 ..."
	cd third_party; tar -xvf htslib-1.3.1.tar.bz2; make -C htslib-1.3.1; cd ..

compile: src/bam_util.cpp \
         src/bam_util_test.cpp \
         src/fasta_reader.cpp \
         src/fasta_reader_test.cpp \
         src/bed_util.cpp \
         src/bed_util_test.cpp \
         src/primer_trie.cpp \
         src/primer_trie_test.cpp \
         src/primer_qc.cpp \
         src/primer_qc_test.cpp \
         panel_qc.cpp \
         variant_counter.cpp
	@echo "Compiling..."
	g++ src/bam_util_test.cpp  src/bam_util.cpp src/bed_util.cpp src/fasta_reader.cpp -o bam_util_test third_party/htslib-1.3.1/libhts.a -lz -lpthread -g -std=c++0x
	g++ src/bed_util_test.cpp src/bed_util.cpp src/bam_util.cpp src/fasta_reader.cpp -o bed_util_test third_party/htslib-1.3.1/libhts.a -lz -lpthread -g -std=c++0x
	g++ src/fasta_reader_test.cpp src/fasta_reader.cpp -o fasta_reader_test third_party/htslib-1.3.1/libhts.a -lz -lpthread -g -std=c++0x
	g++ src/primer_trie_test.cpp src/primer_trie.cpp -o primer_trie_test -std=c++0x
	g++ src/primer_qc_test.cpp src/primer_qc.cpp src/bam_util.cpp src/bed_util.cpp src/fasta_reader.cpp src/primer_trie.cpp -o primer_qc_test third_party/htslib-1.3.1/libhts.a -lz -lpthread -g -std=c++0x
	g++ panel_qc.cpp src/primer_qc.cpp src/bam_util.cpp src/bed_util.cpp src/fasta_reader.cpp src/primer_trie.cpp -o panel_qc third_party/htslib-1.3.1/libhts.a -lz -lpthread -g -std=c++0x
	g++ variant_counter.cpp src/bam_util.cpp src/bed_util.cpp src/fasta_reader.cpp -o variant_counter third_party/htslib-1.3.1/libhts.a -lz -lpthread -g -std=c++0x

test:
	@echo "Testing..."
	@./bam_util_test
	@./bed_util_test
	@./fasta_reader_test
	@./primer_trie_test
	@./primer_qc_test

clean:
	@echo "Cleaning up..."
	@rm ./bam_util_test
	@rm ./bed_util_test
	@rm ./fasta_reader_test
	@rm ./primer_trie_test
	@rm ./primer_qc_test
	@echo "Successfully completed"
